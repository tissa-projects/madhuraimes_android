package com.example.restaurantapp.Utilities.swa;


import com.example.restaurantapp.models.CartCountRequest;
import com.example.restaurantapp.models.RetrarentRequest;
import com.example.restaurantapp.models.allmodel.LoginResponse;
import com.example.restaurantapp.models.allmodel.Loginpara;
import com.example.restaurantapp.models.allmodel.UserRestrout;
import com.example.restaurantapp.models.allmodel.UseridRequest;
import com.example.restaurantapp.models.allmodel.usermodel;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    @Headers("Content-Type: application/json")
    @GET("restaurant/1/")
    Call<RetrarentRequest> fetchdata(
            @Header("Authorization") String authorization
    );

    @POST("graphql/")
    Call<CartCountRequest> cartcount(
            @Body RequestBody query
    );

    @POST("/userrestaurant/")
    Call<UserRestrout> insertuser(
            @Header("Authorization") String authorization,
            @Body usermodel login
    );
    @Headers("Content-Type: application/json")
    @POST("rest-auth/login/v1/")
    Call<LoginResponse> login(
            @Body Loginpara login
    );
    @Headers("Content-Type: application/json")
    @GET("user/?")
    Call<UseridRequest> getuser(
            @Header("Authorization") String authorization,
            @Query("username") String status
    );

}
