package com.example.restaurantapp.interfaces;

public interface AsyncResponse {
    void processResponse(String response);
}
