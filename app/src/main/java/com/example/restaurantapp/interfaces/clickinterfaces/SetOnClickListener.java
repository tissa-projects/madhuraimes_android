package com.example.restaurantapp.interfaces.clickinterfaces;

import org.json.JSONObject;

public interface SetOnClickListener {
    void onCLick() ;
   // void onPasswordChangeClick();
    interface setCardClick { void onClick(int position, JSONObject jsonObject);}
    interface SetPasswordChange { void onPasswordChangeClick();}
    interface SetprofileImage { void onProfileImageClick();}
}
