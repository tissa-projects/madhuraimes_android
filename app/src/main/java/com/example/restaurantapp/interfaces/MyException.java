package com.example.restaurantapp.interfaces;

import android.content.Context;

import com.example.restaurantapp.Utilities.CustomDialogs;


public class MyException extends RuntimeException {

    public MyException(Context ctx) {
        super();
        CustomDialogs.dialogSessionExpire(ctx);
    }

}
