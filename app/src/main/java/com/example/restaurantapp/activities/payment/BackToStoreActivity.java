package com.example.restaurantapp.activities.payment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.activities.dashboard.DashBoardActivity;
import com.example.restaurantapp.activities.myorderdetails.MyOrderDetailsAdapter;
import com.example.restaurantapp.activities.myorderdetails.MyOrderDetailsViewModel;
import com.example.restaurantapp.databinding.ActivityBackToStoreBinding;

import org.json.JSONArray;
import org.json.JSONObject;

public class BackToStoreActivity extends AppCompatActivity {

    private ActivityBackToStoreBinding binding;
    private MyOrderDetailsAdapter adapter;
    private MyOrderDetailsViewModel myOrderDetailsViewModel;
    private Context context;
    private Dialog dialog;
    private String strOrderId, currencyType;

    private static final String TAG = BackToStoreActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_back_to_store);
        myOrderDetailsViewModel = ViewModelProviders.of(this).get(MyOrderDetailsViewModel.class);
        context = BackToStoreActivity.this;
        currencyType = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE);
        getIntentData();
        init();

        binding.btnBackToHome.setOnClickListener(v -> {
            startActivity(new Intent(context, DashBoardActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            Utils.fadeAnimation(context);
        });
    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                Log.e(TAG, "getIntentData: bundle: " + bundle.toString());
                String data = bundle.getString("OrderDetails");
                Log.e(TAG, "getIntentData: data: "+data );
                String addressDetails = bundle.getString("addressDetails");
                JSONObject bundleData = new JSONObject(data);
                JSONObject addressObj = new JSONObject(addressDetails);
                String strOrderDate = Utils.ConvertIntoDateFormat("yyyy-MM-dd", "dd MMMM yyyy", bundleData.getString("created_at"));
                binding.txtOrderDate.setText(strOrderDate);
                binding.txtOrderNo.setText(bundleData.getString("order_id"));
                strOrderId = binding.txtOrderNo.getText().toString();
                binding.txtSubTotal.setText(currencyType + bundleData.getString("subtotal"));
                binding.txtTip.setText(currencyType + bundleData.getString("tip"));
             ///   binding.txtServiceFee.setText(currencyType + bundleData.getString("service_fee"));
                binding.txtTax.setText(currencyType + bundleData.getString("tax"));
             //   binding.txtShippingFee.setText(currencyType + bundleData.getString("shipping_fee"));
                binding.txtDiscount.setText(currencyType + bundleData.getString("discount"));
                binding.txtOrderTotal.setText(currencyType + bundleData.getString("total"));
                binding.txtShippingAddress.setText(addressObj.getString("name") + "\n" + addressObj.getString("address") +" "+  addressObj.getString("house_number") + "\n" +
                        addressObj.getString("city") + "\n" +
                        addressObj.getString("state") + "\n" +
                        addressObj.getString("country") + "\n" +
                        addressObj.getString("zip") + "\n");
                if (VU.isConnectingToInternet(context)) {
                    getOrderDetails(strOrderId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Order Details");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.INVISIBLE);
        adapter = new MyOrderDetailsAdapter(context);
        binding.setMyOrderDetailsAdapter(adapter);

       /* binding.txtOrderDate.setText(strOrderDate);
        binding.txtOrderNo.setText(strOrderId);
        binding.txtOrderTotal.setText("$"+strOrderTotal);*/
    }


    private void getOrderDetails(String strOrderId) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_order_details_api) + strOrderId;
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.get_order_details_api));
            helper.setOrderId(strOrderId);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        myOrderDetailsViewModel.getMyOrderDetails(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getStoreList: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        Log.e(TAG, "getOrderDetails: "+jsonArray );
                        if (jsonArray.length() > 0) {
                            adapter.setData(jsonArray);
                        } else {
                            Utils.getToast(context, getResources().getString(R.string.no_product_available_for_order));
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    /*private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                Log.e(TAG, "getIntentData: bundle: " + bundle.toString());
                String data = bundle.getString("OrderDetails");
                JSONObject bundleData = new JSONObject(data);
                binding.txtOrderNo.setText(bundleData.getString("order_id"));
                binding.txtTip.setText("$"+bundleData.getString("tip"));
                binding.txtServiceFee.setText("$"+bundleData.getString("service_fee"));
                binding.txtTax.setText("$"+bundleData.getString("tax"));
                binding.txtShippingFee.setText("$"+bundleData.getString("shipping_fee"));
                binding.txtDiscount.setText("$"+bundleData.getString("discount"));
                binding.txtOrderTotal.setText("$"+bundleData.getString("total"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/
    @Override
    public void onBackPressed() {
        Utils.getToast(context, getResources().getString(R.string.go_back_not_allow));
    }
}
