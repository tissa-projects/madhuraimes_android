package com.example.restaurantapp.activities.myorders;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.databinding.RecyclerMyOrdersFragmentBinding;
import com.example.restaurantapp.interfaces.clickinterfaces.SetOnClickListener;
import com.example.restaurantapp.models.MyOrderModel;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyOrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private static final String TAG = MyOrdersAdapter.class.getSimpleName();

    private SetOnClickListener.setCardClick setViewDetails, setOrderCancel;
    private JSONArray jsonArray;


    public MyOrdersAdapter(Context context) {
        this.context = context;
        jsonArray = new JSONArray();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerMyOrdersFragmentBinding myOrdersFragmentBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_my_orders_fragment, parent, false);

        return new MyViewHolder(myOrdersFragmentBinding);
    }

     @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            String paymentStatus = null;
            MyViewHolder myViewHolder = null;
            MyOrderModel myOrderModel = new MyOrderModel();
            if (holder instanceof MyViewHolder) {
                myViewHolder = (MyViewHolder) holder;
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                myViewHolder.myOrdersFragmentBinding.txtOrderNo.setText(context.getResources().getString(R.string.order_no)+jsonObject.getJSONObject("order").getString("order_id"));
                myViewHolder.myOrdersFragmentBinding.txtOrderDate.setText(context.getResources().getString(R.string.order_date) + Utils.ConvertIntoDateFormat("yyyy-MM-dd", "dd MMMM yyyy", jsonObject.getString("created_at")));
                myViewHolder.myOrdersFragmentBinding.txtTotal.setText(context.getResources().getString(R.string.total)+jsonObject.getString("amount"));
                myViewHolder.myOrdersFragmentBinding.txtPaymentType.setText(context.getResources().getString(R.string.pymt_type)+jsonObject.getString("payment_method"));
                myViewHolder.myOrdersFragmentBinding.txtStatus.setText(context.getResources().getString(R.string.status)+jsonObject.getString("status"));

                myViewHolder.myOrdersFragmentBinding.setViewDetails(() -> {
                    try {
                        setViewDetails.onClick(position, jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

//                myViewHolder.myOrdersFragmentBinding.setOrderCancel(() -> {
//                    setOrderCancel.onClick(position, jsonObject);
//                });
            }

            myViewHolder.bind(myOrderModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        //  return 10;
        return jsonArray.length();

    }

    public void setData(JSONArray jsonArray) {
        Log.e(TAG, "setData: "+jsonArray );
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerMyOrdersFragmentBinding myOrdersFragmentBinding;

        public MyViewHolder(@NonNull RecyclerMyOrdersFragmentBinding myOrdersFragmentBinding) {
            super(myOrdersFragmentBinding.getRoot());
            this.myOrdersFragmentBinding = myOrdersFragmentBinding;
        }

        public void bind(Object obj) {
            myOrdersFragmentBinding.setMyOrder((MyOrderModel) obj);
            myOrdersFragmentBinding.executePendingBindings();
        }
    }

    public void ViewDetailsClick(SetOnClickListener.setCardClick setOnClickListener) {
        this.setViewDetails = setOnClickListener;
    }

    public void cancelDeleteClick(SetOnClickListener.setCardClick setOnClickListener) {
        this.setOrderCancel = setOnClickListener;
    }


}
