package com.example.restaurantapp.activities.categoryNmenu;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;

public class MenuViewModel extends AndroidViewModel {

    private MenuRepository repository;

    public MenuViewModel(@NonNull Application application) {
        super(application);
        repository = MenuRepository.getInstance(application);
    }

    public MutableLiveData<String> getMenuList(RestAPIClientHelper restAPIClientHelper) {
        return repository.getMenuList(restAPIClientHelper);
    }

    public MutableLiveData<String> getAddToCartData(RestAPIClientHelper restAPIClientHelper) {
        return repository.setAddToCart(restAPIClientHelper);
    }
    public MutableLiveData<String> updateProductDetailsWRTCartItemId(RestAPIClientHelper restAPIClientHelper) {
        return repository.updateProductDetailsWRTCartItemId(restAPIClientHelper);
    }
    public MutableLiveData<String> getProductDetailsWRTCartItemId(RestAPIClientHelper restAPIClientHelper) {
        return repository.getProductDetailsWRTCartItemId(restAPIClientHelper);
    }

    public MutableLiveData<String> getIngredientList(RestAPIClientHelper restAPIClientHelper) {
        return repository.setAddToCart(restAPIClientHelper);
    }

    public MutableLiveData<String> deleteProduct(RestAPIClientHelper restAPIClientHelper) {
        return repository.deleteProduct(restAPIClientHelper);
    }

    public MutableLiveData<String> getCart(RestAPIClientHelper helper){
        return repository.getCart(helper);
    }
    public MutableLiveData<String> createCart(RestAPIClientHelper helper){
        return repository.createCart(helper);
    }

    public LiveData<String> getCartCount(RestAPIClientHelper restAPIClientHelper) {
        return repository.getCartCount(restAPIClientHelper);
    }

    public MutableLiveData<String> getSearchData(RestAPIClientHelper restAPIClientHelper) {
        return repository.getSearchData(restAPIClientHelper);
    }

    public LiveData<String> deleteCart(RestAPIClientHelper restAPIClientHelper) {
        return repository.deleteCart(restAPIClientHelper);
    }
}
