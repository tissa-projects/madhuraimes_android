package com.example.restaurantapp.activities.auth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.Preferences;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.databinding.ActivitySignUpBinding;

import org.json.JSONObject;

import java.util.ArrayList;

public class SignUpActivity extends AppCompatActivity  implements  View.OnClickListener {

    private ActivitySignUpBinding binding;
    private AuthViewModel viewModel;
    private Context context;
    private String CountryID;
    private ArrayList<String> salutationArray = null;
    public static final String TAG = SignUpActivity.class.getSimpleName();
    String token22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        viewModel = ViewModelProviders.of(this).get(AuthViewModel.class);
        context = SignUpActivity.this;
        init();
    }

    private void init() {
        binding.txtAlreadyRegistered.setOnClickListener(this);
        binding.edtSalutation.setOnClickListener(this);
        salutationArray = new ArrayList<>();
        salutationArray.add("Mr.");
        salutationArray.add("Mrs.");
        salutationArray.add("Miss.");
        salutationArray.add("Ms.");

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        CountryID= manager.getSimCountryIso().toUpperCase();
        CountryID = (CountryID.equalsIgnoreCase("IN"))? "+91": "+1";
        binding.edtCode.setText(CountryID);
        binding.btnSignUp.setOnClickListener(v -> {
            if (viewModel.validateRegistartionn(context, binding)) {
                if (VU.isConnectingToInternet(context))
                    checkForLogin();
            }
        });
    }

    //salutation dialog
    private void setSalutation() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Salutation");
        // add a list
        builder.setItems(Utils.GetStringArray(salutationArray), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                binding.edtSalutation.setText(salutationArray.get(position));
                dialog.dismiss();
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_already_registered:
                finish();
                Utils.fadeAnimation(context);
                break;
            case R.id.edt_salutation:
                setSalutation();
                break;
        }
    }

    private void checkForLogin() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.login);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("username", getResources().getString(R.string.app_username));
            stringMap.put("password", getResources().getString(R.string.app_password));
            stringMap.put("restaurant_id", "1");
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.login(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "checkForLogin: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        register(jsonObject.getString("token"));
                        token22=jsonObject.getString("token");

                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void register(String authToken) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            JSONObject jsonReqst = new JSONObject();
            jsonReqst.put("username", binding.edtUserName.getText().toString());
            jsonReqst.put("email", binding.edtEmail.getText().toString());
            jsonReqst.put("first_name", binding.edtFName.getText().toString());
            jsonReqst.put("last_name", binding.edtLName.getText().toString());
            jsonReqst.put("password", binding.edtPassword.getText().toString());
            jsonReqst.put("verified", "N");
            jsonReqst.put("restaurant_id", "1");
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setAuthorization(authToken);
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.register));
            helper.setUrlParameter(jsonReqst.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.setRegistrationData(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    // registrationViewModel.setRegistrationData(helper).removeObservers(this);
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "register: " + responseObj.toString());
                 //   Log.e(TAG, "register: " + responseObj.getString("message"));
                    Log.e(TAG, "registerww: " + Preferences.getstatus());
                    switch (Preferences.getstatus()) {
                        case 200:
                            dialogShowMsg(context,"");
                           // Utils.getToast(context, getResources().getString(R.string.registration_done));
                            UserSession.setuserId(context, responseObj.getString("id"));  //customer id or user_id

                            break;
                        case 400:
//                            if (responseObj.getString("message").toString().equals("username")){
//                                Utils.getToast(context, getResources().getString(R.string.user_already_present));
//                            }else {
                                Utils.getToast(context, getResources().getString(R.string.email_already_present));
//                            }

                            break;
                        default:
                            Utils.getToast(context, getResources().getString(R.string.no_response));
                            break;
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public Dialog dialogShowMsg(Context context, String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.loginsuccefullydiaolog);
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
//        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                createCustomer("");
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        return dialog;
    }


    private void createCustomer(String authToken) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.create_customer_api);
        try {
            JSONObject reqObj = new JSONObject();
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("last_access", Utils.CurrentTimeStamp());
            reqObj.put("extra", "extra");
            reqObj.put("salutation", binding.edtSalutation.getText().toString());
            reqObj.put("phone_number", CountryID+binding.edtMNo.getText().toString());
            reqObj.put("first_name", binding.edtFName.getText().toString());
            reqObj.put("last_name", binding.edtLName.getText().toString());
            reqObj.put("email", binding.edtEmail.getText().toString());
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setAuthorization(token22);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.createCustomer(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "createCustomer: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                       // JSONObject resultObj = jsonObject.getJSONObject("data");
                        if (jsonObject != null) {
                            startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            Utils.fadeAnimation(context);
                        }
                    } else
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.fadeAnimation(context);
    }
}
