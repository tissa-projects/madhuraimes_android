package com.example.restaurantapp.activities.myorderdetails;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.activities.ServiceCall.RestAsyncTask;


public class MyOrderDetailsRepository {

    private static MyOrderDetailsRepository instance;
    private static Application applicationContext;
    private static final String TAG = MyOrderDetailsRepository.class.getSimpleName();

    public static MyOrderDetailsRepository getInstance(Application application) {
        applicationContext = application;
        if (instance == null) {
            instance = new MyOrderDetailsRepository();

        }
        return instance;
    }

    public MutableLiveData<String> getOrderDetails(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(applicationContext.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> getBillingAddress(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(applicationContext.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

}
