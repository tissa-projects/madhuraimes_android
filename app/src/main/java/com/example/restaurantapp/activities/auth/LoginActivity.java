package com.example.restaurantapp.activities.auth;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.Preferences;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.Utilities.swa.RetrofitClient;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.activities.dashboard.DashBoardActivity;
import com.example.restaurantapp.databinding.ActivityLoginBinding;
import com.example.restaurantapp.interfaces.clickinterfaces.SetOnClickListener;
import com.example.restaurantapp.models.allmodel.LoginResponse;
import com.example.restaurantapp.models.allmodel.Loginpara;
import com.example.restaurantapp.models.allmodel.UserRestrout;
import com.example.restaurantapp.models.allmodel.UseridRequest;
import com.example.restaurantapp.models.allmodel.UseridResponse;
import com.example.restaurantapp.models.allmodel.usermodel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements SetOnClickListener, View.OnClickListener {

    private ActivityLoginBinding binding;
    private AuthViewModel viewModel;
    private Context context;
    private String currentVersion, latestVersion;
    private Dialog dialog;
    public static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = ViewModelProviders.of(this).get(AuthViewModel.class);
        context = LoginActivity.this;
        init();
    }

    private void init() {

        binding.forgotPassword.setOnClickListener(this);
        binding.forgotUser.setOnClickListener(this);
        binding.btnGuest.setOnClickListener(v -> {
            startActivity(new Intent(context, MobileVerificationActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });
        binding.btnLogin.setOnClickListener(v -> {
            if (viewModel.validateLogin(context, binding)) {
                if (VU.isConnectingToInternet(context)) {
                    checkForLogin();
                }
            }
        });

        binding.signUp.setOnClickListener(v -> {
            startActivity(new Intent(context, SignUpActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });
    }

    private void checkForLogin() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.login);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("username", binding.edtEmail.getText().toString());
            stringMap.put("password", binding.edtPassword.getText().toString());
            stringMap.put("restaurant_id", "1");

            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.login(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "checkForLogin: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.AUTHORIZATION_TOKEN, jsonObject.getString("token"));
                        Preferences.setToken(jsonObject.getString("token"));
                        UserSession.setIsuserLogin(context);
                        UserSession.setuserId(context, jsonObject.getString("id"));
                        Preferences.setUserId(jsonObject.getString("id"));
                        Intent intent = new Intent(context, DashBoardActivity.class);
                        Preferences.setUserProfile("1");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        Utils.fadeAnimation(context);

                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401)
                        Utils.getToast(context, getResources().getString(R.string.credential_wrong));
                    else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 403) {
                        if (jsonObject.getString("msg").equals("You do not have access to restaurant.")) {
                            retro("", context);
                        } else {
                            Utils.getToast(context, getResources().getString(R.string.email_not_verified));
                        }

//                        Log.d(TAG, "checkForLogin: " + jsonObject.getString("msg"));


                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    void login() {
        Call<LoginResponse> call = RetrofitClient.getInstance().getapi()
                .login(new Loginpara(getResources().getString(R.string.app_username),
                        getResources().getString(R.string.app_password),"1"));
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse p = response.body();


                if (response.code() == 200) {

                    getuserid(p.getToken());

                } else if (response.code() == 401)
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.credential_wrong), Toast.LENGTH_LONG).show();
                else if (response.code() == 403)
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.email_not_verified), Toast.LENGTH_LONG).show();

                else
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.no_response), Toast.LENGTH_LONG).show();


            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });


    }

    void getuserid(String token) {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        //   API.postJson(new FooRequest("kit", "kat"));
        Call<UseridRequest> call = RetrofitClient.getInstance().getapi()
                .getuser("Token " + token, binding.edtEmail.getText().toString());
        call.enqueue(new Callback<UseridRequest>() {
            @Override
            public void onResponse(Call<UseridRequest> call, Response<UseridRequest> response) {


                if (response.code() == 200) {
                    UseridRequest p = response.body();
                    ArrayList<UseridResponse> s = new ArrayList<UseridResponse>(Arrays.asList(response.body().getResults()));
                    int id = s.get(0).getId();
                    fetchdata(id, "1",token);
                    dialog.dismiss();

                } else if (response.code() == 401) {
                    dialog.dismiss();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.credential_wrong), Toast.LENGTH_LONG).show();
                } else if (response.code() == 403) {
                    dialog.dismiss();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.email_not_verified), Toast.LENGTH_LONG).show();

                } else {
                    dialog.dismiss();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.no_response), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<UseridRequest> call, Throwable t) {
                dialog.dismiss();
            }
        });


    }

    void fetchdata(int userid, String retrourent,String token) {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        //   API.postJson(new FooRequest("kit", "kat"));
        Call<UserRestrout> call = RetrofitClient.getInstance().getapi()
                .insertuser("Token " + token, new usermodel(userid,
                        retrourent));
        call.enqueue(new Callback<UserRestrout>() {
            @Override
            public void onResponse(Call<UserRestrout> call, Response<UserRestrout> response) {


                if (response.code() == 201) {
                    UserRestrout p = response.body();
                    //  ArrayList<RetrarentResponse> s = new ArrayList<RetrarentResponse>(Arrays.asList(response.body().getResults()));

                    checkForLogin();
                    dialog.dismiss();

                } else if (response.code() == 401) {
                    dialog.dismiss();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.credential_wrong), Toast.LENGTH_LONG).show();
                } else if (response.code() == 403) {
                    dialog.dismiss();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.email_not_verified), Toast.LENGTH_LONG).show();

                } else {
                    dialog.dismiss();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.no_response), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<UserRestrout> call, Throwable t) {
                dialog.dismiss();
            }
        });


    }

    public void retro(String type, Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.retro_dilog);

        TextView edtEmail = dialog.findViewById(R.id.txt_msg);

        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // if (type.equalsIgnoreCase("password"))
        edtEmail.setText("You do not have access to this restaurant.You want to login for this restaurant please click on OK.");

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener((view) -> {
            if (VU.isConnectingToInternet(context)) {
                login();
            }
        });

        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener((view) -> {
            dialog.dismiss();
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    protected void ForgotPassowrd(String username) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.forgot_password_api);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("username", username);
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.forgotPasword(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "ForgotPassowrd: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        Utils.getToast(context, getResources().getString(R.string.password_snt));
                    } else {
                        CustomDialogs.dialogShowMsg(context, jsonObject.getString("msg"));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    protected void ForgotUser(String email) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.forgot_user_api);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("email", email);
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.forgotPasword(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "ForgotUser: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        Utils.getToast(context, getResources().getString(R.string.username_snt));
                    } else {
                        CustomDialogs.dialogShowMsg(context, jsonObject.getString("msg"));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_up:
                startActivity(new Intent(context, SignUpActivity.class));
                Utils.fadeAnimation(context);
                break;
            case R.id.forgot_password:
                viewModel.dialogForgotPasswordUser("password", context);
                break;
            case R.id.forgot_user:
                viewModel.dialogForgotPasswordUser("user", context);
                break;
        }
    }

    // login btn Click
    @Override
    public void onCLick() {
        if (viewModel.validateLogin(context, binding)) {
            if (VU.isConnectingToInternet(context)) {
                checkForLogin();
            }
        }
    }
}
