package com.example.restaurantapp.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.Preferences;
import com.example.restaurantapp.Utilities.swa.RetrofitClient;
import com.example.restaurantapp.models.RetrarentRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactUsActivity extends AppCompatActivity {

    private Context context;
    private Dialog dialog;
    public static final String TAG = ContactUsActivity.class.getSimpleName();
    TextView txtcall, txt_mail, txt_location,txt_workhour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        context = ContactUsActivity.this;
//        WebView myWebView = binding.webviewContactUs;
//
//        splashTime();
//        WebSettings webSettings = myWebView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
//        myWebView.setWebViewClient(new WebViewClient());
//        myWebView.loadUrl(getResources().getString(R.string.contact_us));

        txtcall = findViewById(R.id.txtcall);
        txt_mail = findViewById(R.id.txt_mail);
        txt_location = findViewById(R.id.txt_location);
        txt_workhour= findViewById(R.id.txt_workhour);
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Contact Us");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        fetchdata();
    }


    void fetchdata() {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        //   API.postJson(new FooRequest("kit", "kat"));
        Call<RetrarentRequest> call = RetrofitClient.getInstance().getapi()
                .fetchdata("Token " + Preferences.getToken());
        call.enqueue(new Callback<RetrarentRequest>() {
            @Override
            public void onResponse(Call<RetrarentRequest> call, Response<RetrarentRequest> response) {



                if (response.code() == 200) {
                    RetrarentRequest p = response.body();
                  //  ArrayList<RetrarentResponse> s = new ArrayList<RetrarentResponse>(Arrays.asList(response.body().getResults()));
                    txtcall.setText(p.getPhone());
                    txt_mail.setText(p.getEmail());
                    txt_workhour.setText(p.getWorkingHours());
                    String w=p.getAddress()+" "+p.getCity()+", "+p.getCountry()+" "+p.getZip();
                    txt_location.setText(w);
                    dialog.dismiss();

                } else if (response.code() == 401){
                    dialog.dismiss();
                    Toast.makeText(ContactUsActivity.this, getResources().getString(R.string.credential_wrong), Toast.LENGTH_LONG).show();
                }

                else if (response.code() == 403){
                    dialog.dismiss();
                    Toast.makeText(ContactUsActivity.this, getResources().getString(R.string.email_not_verified), Toast.LENGTH_LONG).show();

                }

                else{
                    dialog.dismiss();
                    Toast.makeText(ContactUsActivity.this, getResources().getString(R.string.no_response), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<RetrarentRequest> call, Throwable t) {
                dialog.dismiss();
            }
        });


    }
    public void splashTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1300);
    }

}
