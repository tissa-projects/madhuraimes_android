package com.example.restaurantapp.activities.dashboard;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.example.restaurantapp.R;
import com.example.restaurantapp.SliderItem;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.Preferences;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.Utilities.swa.MyApp;
import com.example.restaurantapp.Utilities.swa.RetrofitClient;
import com.example.restaurantapp.activities.AboutUsActivity;
import com.example.restaurantapp.activities.ContactUsActivity;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.activities.auth.AuthViewModel;
import com.example.restaurantapp.activities.auth.LoginActivity;
import com.example.restaurantapp.activities.cart.CartActivity;
import com.example.restaurantapp.activities.categoryNmenu.MenuDetailsActivity;
import com.example.restaurantapp.activities.myorders.MyOrdersActivity;
import com.example.restaurantapp.activities.profile.ProfileActivity;
import com.example.restaurantapp.databinding.ActivityDashboardBinding;
import com.example.restaurantapp.models.CartCountRequest;
import com.example.restaurantapp.models.CategoryMenuModel;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;
import com.nightonke.boommenu.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardActivity extends AppCompatActivity {

    private ActivityDashboardBinding binding;
    private ViewPager2 viewPager2;
    private RelativeLayout rlViewPager;
    private List<SliderItem> sliderItemList;
    private Context context;
    private DashboardAdapter adapter;
    private CategoryAdapter categoryAdapter;
    private Map<Integer, CategoryMenuModel> categoryMenuModelMap;
    private AuthViewModel viewModel;
    private SearchView searchView;
    private JSONArray menuArray;
    private SpecialMenuAapter sliderAapter;
    private RecyclerView recyclerView1, recyclerCategory;

    private Handler sliderHandler = new Handler();
    public static final String TAG = DashBoardActivity.class.getSimpleName();
    private String[] headerMenu = {"My Profile", "My Orders", "About Us", "Contact Us", "Logout"};
    private int[] headerImage = {R.drawable.profile, R.drawable.my_order, R.drawable.about, R.drawable.contact_us, R.drawable.logout};

    private String[] headerMenu1 = {"My Orders", "About Us", "Contact Us", "Logout"};
    private int[] headerImage1 = {R.drawable.my_order, R.drawable.about, R.drawable.contact_us, R.drawable.logout};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_dashboard);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        viewModel = ViewModelProviders.of(this).get(AuthViewModel.class);
        context = DashBoardActivity.this;
        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CURRENCY_TYPE, "$");
        init();
        initRecycler();
        initCategoryRecycler();
        if (VU.isConnectingToInternet(context)) {
            restaurantHour();
            CategoryList();
            cartcount();
//            if (!SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID).equalsIgnoreCase("")) {
//                cartcount();  //cart item count
//            } else {
//                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
//            }
        }
    }

    private void init() {
        Preferences.appContext = getApplicationContext();
        viewPager2 = findViewById(R.id.view_pager);
        rlViewPager = findViewById(R.id.rl_view_pager);
        searchView = findViewById(R.id.search_box);
        recyclerView1 = findViewById(R.id.recycler_dashboard1);
        recyclerCategory = findViewById(R.id.category_recycler);
        binding.setCartClick(this::onCLick);
        categoryMenuModelMap = new HashMap<>();
        menuArray = new JSONArray();

        sliderItemList = new ArrayList<>();
        sliderItemList.add(new SliderItem(R.drawable.store));
        sliderItemList.add(new SliderItem(R.drawable.store));
        sliderItemList.add(new SliderItem(R.drawable.store));
        sliderItemList.add(new SliderItem(R.drawable.store));
        sliderItemList.add(new SliderItem(R.drawable.store));

        sliderAapter = new SpecialMenuAapter(context, viewPager2);
        // viewPager2.setAdapter(new SpecialMenuAapter(sliderItemList, viewPager2));
        viewPager2.setAdapter(sliderAapter);
        viewPager2.setClipToPadding(false);
        viewPager2.setClipChildren(false);
        viewPager2.setOffscreenPageLimit(3);
        //   viewPager2.setCurrentItem(1, true);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float r = 1 - Math.abs(position);
                page.setScaleY(0.85f + r * 0.15f);
            }
        });
        viewPager2.setPageTransformer(compositePageTransformer);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                sliderHandler.removeCallbacks(sliderRunner);
                sliderHandler.postDelayed(sliderRunner, 2000); //slide duration 3 sec
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do your search
                adapter.clearData();
                searchProduct();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    Utils.getToast(context, "Add Menu to search");
                }
                return false;
            }
        });
    }

    private void initCategoryRecycler() {
        recyclerCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        categoryAdapter = new CategoryAdapter(context);
        recyclerCategory.setAdapter(categoryAdapter);

        // on item list clicked
        categoryAdapter.setOnItemClickListener((JSONObject obj, int position) -> {
          /*  recyclerCategory.post(new Runnable() {
                @Override
                public void run() {
                    recyclerCategory.scrollToPosition(adapter.getItemCount() - 1);
                    // Here adapter.getItemCount()== child count
                }
            });*/
            try {
                adapter.clearData();
                if (position == 0) {
                    getSpecialMenu();
                } else {
                    getMenuList(obj.getString("category_id"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    private void initRecycler() {
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(context);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setAlignItems(AlignItems.STRETCH);
        recyclerView1.setLayoutManager(layoutManager);
        adapter = new DashboardAdapter(context);
        recyclerView1.setAdapter(adapter);

        // on item list clicked
        adapter.setOnItemClickListener((JSONObject obj, int position) -> {
            startActivity(new Intent(context, MenuDetailsActivity.class).putExtra("data", obj.toString()));
            Utils.fadeAnimation(context);

        });
        if (Preferences.getUserProfile().equals("1")) {
            setBoomMenu();
        } else {
            setBoomMenu1();
        }


    }

    private void setBoomMenu1() {
        //setting the menu items using the BoomMenu
        BoomMenuButton rightBmb = findViewById(R.id.action_bar_right_bmb);
        rightBmb.setButtonEnum(ButtonEnum.Ham);
        rightBmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_4);
        rightBmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_4);
        for (int i = 0; i < rightBmb.getPiecePlaceEnum().pieceNumber(); i++) {
            HamButton.Builder builder = new HamButton.Builder()
                    .normalImageRes(headerImage1[i])
                    .imagePadding(new Rect(15, 4, 15, 5))
                    .pieceColorRes(R.color.colorPrimary)
                    .normalColorRes(R.color.colorPrimary)
                    .imageRect(new Rect(Util.dp2px(5), Util.dp2px(10), Util.dp2px(50), Util.dp2px(50)))
                    .normalText(headerMenu1[i])
                    .subNormalTextColor(R.color.colorPrimary);
            //       .subNormalText(getResources().getString(subHeaderMenu[i]));
            rightBmb.addBuilder(builder);
        }

        //onclick event of the BoomMenu
        rightBmb.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                switch (index) {
//                    case 0: {
//
//                        startActivity(new Intent(context, ProfileActivity.class));
//                        break;
//                    }
                    /*case 1: {
                        startActivity(new Intent(context, CartActivity.class));
                        break;
                    }*/
                    case 0: {
                        startActivity(new Intent(context, MyOrdersActivity.class));
                        break;
                    }
                    case 1: {
                        startActivity(new Intent(context, AboutUsActivity.class));
                        break;
                    }
                    case 2: {
                        startActivity(new Intent(context, ContactUsActivity.class));
                        break;
                    }
                    case 3: {
                        dialogLogout();
                        break;
                    }
                    default: {
                        Toast.makeText(DashBoardActivity.this, "Work under process", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }

            @Override
            public void onBackgroundClick() {

            }

            @Override
            public void onBoomWillHide() {

            }

            @Override
            public void onBoomDidHide() {

            }

            @Override
            public void onBoomWillShow() {

            }

            @Override
            public void onBoomDidShow() {

            }
        });
    }

    private void setBoomMenu() {
        //setting the menu items using the BoomMenu
        BoomMenuButton rightBmb = findViewById(R.id.action_bar_right_bmb);
        rightBmb.setButtonEnum(ButtonEnum.Ham);
        rightBmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_5);
        rightBmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_5);
        for (int i = 0; i < rightBmb.getPiecePlaceEnum().pieceNumber(); i++) {
            HamButton.Builder builder = new HamButton.Builder()
                    .normalImageRes(headerImage[i])
                    .imagePadding(new Rect(15, 4, 15, 5))
                    .pieceColorRes(R.color.colorPrimary)
                    .normalColorRes(R.color.colorPrimary)
                    .imageRect(new Rect(Util.dp2px(5), Util.dp2px(10), Util.dp2px(50), Util.dp2px(50)))
                    .normalText(headerMenu[i])
                    .subNormalTextColor(R.color.colorPrimary);
            //       .subNormalText(getResources().getString(subHeaderMenu[i]));
            rightBmb.addBuilder(builder);
        }

        //onclick event of the BoomMenu
        rightBmb.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                switch (index) {
                    case 0: {

                        startActivity(new Intent(context, ProfileActivity.class));
                        break;
                    }/*case 1: {
                        startActivity(new Intent(context, CartActivity.class));
                        break;
                    }*/
                    case 1: {
                        startActivity(new Intent(context, MyOrdersActivity.class));
                        break;
                    }
                    case 2: {
                        startActivity(new Intent(context, AboutUsActivity.class));
                        break;
                    }
                    case 3: {
                        startActivity(new Intent(context, ContactUsActivity.class));
                        break;
                    }
                    case 4: {
                        dialogLogout();
                        break;
                    }
                    default: {
                        Toast.makeText(DashBoardActivity.this, "Work under process", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }

            @Override
            public void onBackgroundClick() {

            }

            @Override
            public void onBoomWillHide() {

            }

            @Override
            public void onBoomDidHide() {

            }

            @Override
            public void onBoomWillShow() {

            }

            @Override
            public void onBoomDidShow() {

            }
        });
    }


    private Runnable sliderRunner = new Runnable() {
        @Override
        public void run() {
            viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        sliderHandler.removeCallbacks(sliderRunner);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sliderHandler.postDelayed(sliderRunner, 2000);
        viewPager2.setCurrentItem(1, true);
        if (!SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID).equalsIgnoreCase("")) {
            //  cartCount();  //cart item count
            cartcount();
        }
    }

    private void CategoryList() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.category);   //restaurant id change depending upon restaurant from api_list.xml
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.login(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    Log.e(TAG, "CategoryList: " + response);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray jsonArray = new JSONArray(response);
                        Log.e(TAG, "CategoryList: " + jsonArray.toString());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            categoryMenuModelMap.put(jsonArray.getJSONObject(i).getInt("category_id"), new CategoryMenuModel(jsonArray.getJSONObject(i)));
                        }
                        //    adapter.setData(jsonArray);
                        categoryAdapter.setData(jsonArray);
                        getMenuList("");
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        JSONObject jsonObject = new JSONObject(response);
                        Utils.getToast(context, jsonObject.getString("msg"));  // internal server error
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                Utils.getToast(context, getResources().getString(R.string.no_response));
                e.printStackTrace();
            }
        });
    }

    private void getMenuList(String categoryId) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            if (categoryId.equalsIgnoreCase("")) {
                helper.setRequestUrl(getResources().getString(R.string.main_url) + "" + getResources().getString(R.string.menu_list_all));
            } else {
                helper.setRequestUrl(getResources().getString(R.string.main_url) + "" + getResources().getString(R.string.menu_list) + categoryId);
            }
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getMenuList(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        Log.e(TAG, "getMenuList: " + jsonArray);
                        if (jsonArray.length() == 0) {
                            Utils.getToast(context, getResources().getString(R.string.no_catagories));
                        }
                        adapter.setData(jsonArray, categoryMenuModelMap, "menu_list");

                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        JSONObject jsonObject = new JSONObject(response);
                        Utils.getToast(context, jsonObject.getString("msg"));  // internal server error
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                Utils.getToast(context, getResources().getString(R.string.no_response));
                e.printStackTrace();
            }
        });
    }


    private void getSpecialMenu() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + "" + getResources().getString(R.string.special_menu_api));
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getSpecialMenu(helper).observe(this, (response) -> {
            try {
                Log.e(TAG, "getSpecialMenu: " + response);
                dialog.dismiss();
                adapter.clearData();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray jsonArray = new JSONArray(response);
                        if (jsonArray.length() > 0) {
                            adapter.setSpecialMenu(jsonArray);
                        } else {
                            rlViewPager.setVisibility(View.GONE);
                        }
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        JSONObject jsonObject = new JSONObject(response);
                        Utils.getToast(context, jsonObject.getString("msg"));  // internal server error
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                Utils.getToast(context, getResources().getString(R.string.no_response));
                e.printStackTrace();
            }
        });
    }

    private void searchProduct() {
        String productName = searchView.getQuery().toString();

        String query = "query { productSearch(token :\"" + getResources().getString(R.string.graphql_token) +
                "\", productName:\"" + productName +
                "\", restaurantId:" + getResources().getString(R.string.restaurant) +
                //      "\",categoryId:" + Integer.valueOf(categoryId) +
                // ") { productId productName productCategory productUrl price taxExempt categoryId} }";
                ") { productId productName productUrl price taxExempt extra  category{ categoryId category }} }";
        /*query{productSearch(token:\"\(GlobalObjects.globGraphQlToken)\", productName :\"\(searchdata)\", restaurantId : \(restaurantid) , extra : \"\(extra)\" , first:12, skip:\(page))" +
            "{\n productId\n productName\n  productUrl\n price\n taxExempt\n} \n}"*/
        JSONObject jsonObject3 = new JSONObject();
        try {
            jsonObject3.put("query", query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(getResources().getString(R.string.main_url) + "graphql/");
            helper.setUrlParameter(jsonObject3.toString());
            helper.setAction(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getSearchData(helper).observe(this, (response) -> {
            try {
                Log.e(TAG, "searchProduct: " + response);
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    adapter.clearData();
                    JSONObject jsonObject = new JSONObject(response);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        Log.e(TAG, "graphQlEx: " + jsonObject.toString());
                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("productSearch");
                        if (jsonArray.length() > 0) {
                            adapter.setData(jsonArray, categoryMenuModelMap, "search");
                        } else {
                            adapter.setData(new JSONArray(), categoryMenuModelMap, "search");  // biank array for  no prooduct
                            Utils.getToast(context, getResources().getString(R.string.no_product_for_search));
                        }
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void dialogLogout() {
        Dialog dialog = CustomDialogs.dialogCancelOkBtn(context, getResources().getString(R.string.are_you_sure_want_to_logout));
        Button okbtn = dialog.findViewById(R.id.btn_ok);
        Button cancelbtn = dialog.findViewById(R.id.btn_cancel);
        okbtn.setText("Yes");
        cancelbtn.setText("No");
        okbtn.setOnClickListener(v1 -> {
            if (VU.isConnectingToInternet(context)) {
                logout();
                dialog.dismiss();
            }
        });
        cancelbtn.setOnClickListener(v1 -> {
            dialog.dismiss();
        });
    }

    private void logout() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.logout_api);
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.logout(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.e(TAG, "logout: " + jsonObject.toString());

                        UserSession.logoutUser(context, getResources().getString(R.string.logout));
                        SharePreferenceUtil.clear(context);
                        startActivity(new Intent(context, LoginActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        Utils.fadeAnimation(context);
                        //   navController.setGraph(R.navigation.mobile_navigation, null);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        JSONObject jsonObject = new JSONObject(response);
                        Utils.getToast(context, jsonObject.getString("msg"));  // internal server error
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }

                }
            } catch (Exception e) {
                Utils.getToast(context, getResources().getString(R.string.no_response));
                e.printStackTrace();
            }
        });
    }

    //cart item count
//    private void cartCount() {
//        RestAPIClientHelper helper = new RestAPIClientHelper();
//        try {
//            helper.setContentType("application/json");
//            helper.setMethodType(getResources().getString(R.string.GET));
//            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_item) + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID) + "&status=ACTIVE");
//            helper.setUrlParameter("");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
//        viewModel.getCartCount(helper).observe(this, (response) -> {
//            try {
//                dialog.dismiss();
//                if (response == null) {
//                    CustomDialogs.dialogRequestTimeOut(context);
//                } else {
//                    JSONObject jsonObject = new JSONObject(response);
//                    Log.e(TAG, "cartCount: " + jsonObject);
//                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
//                        JSONArray resultArray = jsonObject.getJSONArray("results");
//                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, String.valueOf(resultArray.length()));
//                        binding.cartcount.setText(SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT));
//                    } else
//                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, String.valueOf(0));
//                       // Utils.getToast(context, getResources().getString(R.string.no_response));
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        });
//    }

//

    void cartcount() {

        //    String s = "{\"query\":\"query cartItemCount ($token: String, $customerId: Int) {cartItemCount (token : $token,customerId: $customerId ) {count}}\",\"variables\":{\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\",\"customerId\":" + Preferences.getUserId() + "}}";
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\"query\":\"query cartItemCount ($token: String, $customerId: Int,$restaurantId: Int) {\\n  cartItemCount (token : $token,customerId: $customerId,restaurantId: $restaurantId ) {\\n    count\\n  }\\n}\",\"variables\":{\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\",\"customerId\":" + Preferences.getUserId() + ",\"restaurantId\":1}}");

        //  RequestBody body = RequestBody.create(mediaType, "{\"query\":\"query cartItemCount ($token: String, $customerId: Int) {\\n  cartItemCount (token : $token,customerId: $customerId ) {count}\\n}\",\"variables\":{\"token\":\"0o6jcui8mfhmp56we69kcmu5rkejtock\",\"customerId\":" + Preferences.getUserId() + ", restaurantId:" + getResources().getString(R.string.restaurant)+ "}}");
        //   API.postJson(new FooRequest("kit", "kat"));
        Call<CartCountRequest> call = RetrofitClient.getInstance().getapi()
                .cartcount(body);
        call.enqueue(new Callback<CartCountRequest>() {
            @Override
            public void onResponse(Call<CartCountRequest> call, Response<CartCountRequest> response) {
                CartCountRequest p = response.body();


                if (response.code() == 200) {

                    SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, String.valueOf(p.getData().getCartItemCount().get("count")));
                    binding.cartcount.setText(SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT));

                } else if (response.code() == 401) {
                    Intent intent = new Intent(MyApp.getContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                    binding.cartcount.setText(SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT));
                }

//                    Toast.makeText(getContext(), getResources().getString(R.string.no_response), Toast.LENGTH_LONG).show();


            }

            @Override
            public void onFailure(Call<CartCountRequest> call, Throwable t) {

            }
        });


    }

    //restaurant status
    private void restaurantHour() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.restaurant_hour)
                    + getResources().getString(R.string.restaurant));
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getCartCount(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "restaurantHour: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("closed")) {
                            binding.restaurantStatus.setVisibility(View.VISIBLE);
                            binding.restaurantStatus.setText(status);
                            //  binding.rlSpecialMenu.setAlpha(Float.valueOf((float) 0.3));
                        }
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public String getRestaurentStatus() {
        return binding.restaurantStatus.getText().toString();
    }

    //  @Override  //cart_app_icon click
    public void onCLick() {
        startActivity(new Intent(context, CartActivity.class));
        Utils.fadeAnimation(context);
    }
}
