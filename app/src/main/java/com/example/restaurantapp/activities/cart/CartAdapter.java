package com.example.restaurantapp.activities.cart;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.databinding.RecyclerCartActivityBinding;
import com.example.restaurantapp.interfaces.clickinterfaces.SetOnClickListener;
import com.example.restaurantapp.models.CartModel;

import org.json.JSONArray;
import org.json.JSONObject;

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private static final String TAG = CartAdapter.class.getSimpleName();
    private SetOnClickListener.setCardClick setOnClickListenerForCardView;
    private OnvalueChangeListener onvalueChangeListener;
    private JSONArray jsonArray;

    public interface OnvalueChangeListener {
        void onItemClick(MyViewHolder myViewHolder, int oldValue, int newValue,JSONObject jsonObject);
    }

    public void setOnIngredientQtyListener(final OnvalueChangeListener onvalueChangeListener) {
        this.onvalueChangeListener = onvalueChangeListener;
    }

    public CartAdapter(Context context) {
        this.context = context;
        jsonArray = new JSONArray();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerCartActivityBinding recyclerCatalogActivityBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_cart_activity, parent, false);

        return new MyViewHolder(recyclerCatalogActivityBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        try {
            if (holder instanceof MyViewHolder) {
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                String strIngredient = "";
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                JSONArray ingredientarray = jsonObject.getJSONArray("ingredient");
                CartModel cartModel = new CartModel();
                cartModel.setProductName(jsonObject.getString("product_name"));
                cartModel.setProductQty(jsonObject.getString("quantity"));

                for (int i = 0;i<ingredientarray.length();i++){
                    strIngredient = strIngredient+ (i+1)+"."+ingredientarray.getJSONObject(i).getString("ingredient_name")+" : \n"+
                                    "    Price :$"+ingredientarray.getJSONObject(i).getString("line_total")+" \n"+
                                    "    Qty : "+ingredientarray.getJSONObject(i).getString("quantity")+"\n";
                }
                if (ingredientarray.length()>0){
                    myViewHolder.recyclerCartActivityBinding.llIngredient.setVisibility(View.VISIBLE);
                }else {
                    myViewHolder.recyclerCartActivityBinding.llIngredient.setVisibility(View.GONE);
                }
                myViewHolder.recyclerCartActivityBinding.txtQty.setNumber(cartModel.getProductQty());
                myViewHolder.recyclerCartActivityBinding.txtDetailSeeMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (myViewHolder.recyclerCartActivityBinding.txtDetailSeeMore.getText().toString().equals("See More")) {
                            myViewHolder.recyclerCartActivityBinding.txtIngredient.setMaxLines(Integer.MAX_VALUE);//your TextView
                            myViewHolder.recyclerCartActivityBinding.txtDetailSeeMore.setText("Showless");
                        } else {
                            myViewHolder.recyclerCartActivityBinding.txtIngredient.setMaxLines(3);//your TextView
                            myViewHolder.recyclerCartActivityBinding.txtDetailSeeMore.setText("See More");
                        }
                    }
                });
                cartModel.setIngredients(strIngredient);
                myViewHolder.recyclerCartActivityBinding.txtItemCost.setText(context.getResources().getString(R.string.currency)+jsonObject.getString("line_total"));
                //cartModel.setProductPrice(context.getResources().getString(R.string.currency)+jsonObject.getString("line_total"));
                Utils.displayImageOriginalString(context,myViewHolder.recyclerCartActivityBinding.iconDash2,jsonObject.getString("product_url"));
                myViewHolder.bind(cartModel);
                Log.e(TAG, "onBindViewHolder: " + jsonObject);
                myViewHolder.recyclerCartActivityBinding.setDeleteClick(() -> {
                    setOnClickListenerForCardView.onClick(position, jsonObject);
                });

                myViewHolder.recyclerCartActivityBinding.txtQty.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
                    @Override
                    public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                        Log.d(TAG, String.format("oldValue: %d   newValue: %d", oldValue, newValue));
                        onvalueChangeListener.onItemClick(myViewHolder,oldValue,newValue,jsonObject);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
      //    return 10;
        return jsonArray.length();

    }

    public void setData(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerCartActivityBinding recyclerCartActivityBinding;

        public MyViewHolder(@NonNull RecyclerCartActivityBinding recyclerCartActivityBinding) {
            super(recyclerCartActivityBinding.getRoot());
            this.recyclerCartActivityBinding = recyclerCartActivityBinding;
        }

        public void bind(Object obj) {
            recyclerCartActivityBinding.setCartModel((CartModel) obj);
            recyclerCartActivityBinding.executePendingBindings();
        }
    }

    public void deleteClick(SetOnClickListener.setCardClick setOnClickListener) {
        this.setOnClickListenerForCardView = setOnClickListener;
    }

}