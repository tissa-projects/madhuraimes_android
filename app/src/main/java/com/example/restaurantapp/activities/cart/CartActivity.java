package com.example.restaurantapp.activities.cart;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.activities.Address.BillingAddressActivity;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.activities.categoryNmenu.MenuViewModel;
import com.example.restaurantapp.databinding.ActivityCartBinding;
import com.example.restaurantapp.interfaces.clickinterfaces.SetOnClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CartActivity extends AppCompatActivity implements SetOnClickListener {

    private ActivityCartBinding binding;
    private MenuViewModel viewModel;
    private Context context;
    private double subtotal = 0;
    private CartAdapter cartAdapter;
    private JSONArray cartDataArray;
    public static final String TAG = CartActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cart);
        viewModel = ViewModelProviders.of(this).get(MenuViewModel.class);
        context = CartActivity.this;
        init();
        initRecycler();
    }


    private void init() {
        binding.setPlaceOrderBtn(this::onCLick);
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("My Cart");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        if (VU.isConnectingToInternet(context)) {
            if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID).equalsIgnoreCase(""))
                getCart();
            else showCartList();
        }
    }


    private void initRecycler() {

        cartAdapter = new CartAdapter(context);
        cartAdapter.deleteClick((position, jsonObject) -> {   // recycler card view click
            dialogDeleteCartItem(jsonObject);
        });

        cartAdapter.setOnIngredientQtyListener(new CartAdapter.OnvalueChangeListener() {
            @Override
            public void onItemClick(CartAdapter.MyViewHolder myViewHolder, int oldValue, int newValue, JSONObject jsonObject) {
                getProductDetailsWRTCartItemId(myViewHolder, jsonObject, oldValue, newValue);
            }
        });
        binding.setCartAdapter(cartAdapter);
    }

    @Override //place order btn
    public void onCLick() {
        if (binding.txtCartTotalAmt.getText().toString().substring(1).equalsIgnoreCase("0")) {
            Utils.getToast(context, getResources().getString(R.string.no_items_to_place_order));
        } else {
            if (VU.isConnectingToInternet(context))
                Log.e(TAG, "onCLick: cartDataArray: " + cartDataArray);
            subtotal  =Double.valueOf(binding.txtCartTotalAmt.getText().toString().replace("$",""));
            Log.e(TAG, "onCLick: subtotal: " + subtotal);
            startActivity(new Intent(context, BillingAddressActivity.class)
                    .putExtra("cartDataArray", cartDataArray.toString())
                    .putExtra("subTotal", String.valueOf(subtotal)));
        }
    }

    public void dialogDeleteCartItem(JSONObject jsonObject) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_delete);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        ((TextView) dialog.findViewById(R.id.txt)).setText(getResources().getString(R.string.dialog_delete_item_txt));
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (VU.isConnectingToInternet(context))
                    deleteCart(jsonObject);
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    private void showCartList() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_item) + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID) + "&status=ACTIVE");
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_cart_item));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getAddToCartData(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "showCartList: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        if (jsonObject.length() == 0) {
                            subtotal = 0;
                            binding.recyclerViewCart.setAdapter(null);

                            binding.txtCartItemCount.setText("(0 Items)");
                            binding.txtCartTotalAmt.setText(getResources().getString(R.string.currency) + "0");
                            Utils.getToast(context, getResources().getString(R.string.no_items_in_cart));
                        } else {
                            JSONArray resultArray = jsonObject.getJSONArray("results");
                            Log.e(TAG, "showCartList: resultArray: " + resultArray);
                            if (resultArray.length() > 0) {
                                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, jsonObject.getString("count"));
                                binding.txtCartItemCount.setText("(" + resultArray.length() + " Items)");
                                for (int i = 0; i < resultArray.length(); i++) {
                                    subtotal += (Double.valueOf(resultArray.getJSONObject(i).getString("line_total")));
                                    JSONArray ingredientArry = resultArray.getJSONObject(i).getJSONArray("ingredient");
                                    if (ingredientArry.length() > 0) {
                                        for (int j = 0; j < ingredientArry.length(); j++) {
                                            subtotal += (Double.valueOf(ingredientArry.getJSONObject(j).getString("line_total")));
                                        }
                                    }
                                }
                                binding.txtCartTotalAmt.setText(getResources().getString(R.string.currency) + Utils.convertToUSDFormat((subtotal + "")));
                                //  cartBinding.txtVat.setText(Utils.convertToUSDFormat(subtotal * 0.08 + ""));
                                // cartBinding.txtFinalPayAmt.setText(Utils.convertToUSDFormat(String.valueOf(subtotal + (subtotal * 0.08))));
                                Log.e(TAG, "showCartList: subtotal: " + subtotal);
                                cartDataArray = resultArray;
                                cartAdapter.setData(resultArray);
                            } else {
                                subtotal = 0;
                                binding.recyclerViewCart.setAdapter(null);
                                binding.txtCartItemCount.setText("(0 Items)");
                                binding.txtCartTotalAmt.setText(getResources().getString(R.string.currency) + "0");
                                Utils.getToast(context, getResources().getString(R.string.no_items_in_cart));
                            }
                        }
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }


    private void getProductDetailsWRTCartItemId(CartAdapter.MyViewHolder myViewHolder, JSONObject jsonObj, int oldValue, int newValue) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) +
                    getResources().getString(R.string.get_cart_item_wrt_cart_item_id) + jsonObj.getString("cart_item_id"));
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_cart_item));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getProductDetailsWRTCartItemId(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getProductDetailsWRTCartItemId: " + jsonObject);
                    Log.e(TAG, "getProductDetailsWRTCartItemId: code : " + SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE));
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        updateProductDetailsWRTCartItemId(myViewHolder, jsonObject, oldValue, newValue);
                    } else {
                        myViewHolder.recyclerCartActivityBinding.txtQty.setNumber(oldValue + "");
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                myViewHolder.recyclerCartActivityBinding.txtQty.setNumber(oldValue + "");
                e.printStackTrace();
            }
        });
    }


    private void updateProductDetailsWRTCartItemId(CartAdapter.MyViewHolder myViewHolder, JSONObject jsonObj, int oldValue, int newValue) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            JSONObject reqObj = new JSONObject();
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(getResources().getString(R.string.main_url) +
                    getResources().getString(R.string.get_cart_item_wrt_cart_item_id) + jsonObj.getString("id")+"/");

           /* productObj.put("product_id", proObj.getString("product_id")).put("product_name", proObj.getString("product_name"))
                    .put("product_url", proObj.getString("product_url")).put("price", proObj.getString("price"))
                    .put("media", proObj.getString("media")).put("caption", proObj.getString("caption"))
                    .put("extra", proObj.getString("extra")).put("image", proObj.getString("image"))
                    .put("status", proObj.getString("status")).put("tax_exempt", proObj.getString("tax_exempt"))
                    .put("restaurant", proObj.getString("restaurant")).put("category", proObj.getString("category"));*/

            reqObj.put("id", jsonObj.getString("id"))
                    .put("product_id", jsonObj.getJSONObject("product").getString("product_id"))
                    .put("extra", "").put("ingredient_id", jsonObj.getString("ingredient_id"))
                    .put("sequence_id", jsonObj.getString("sequence_id")).put("cart", jsonObj.getString("cart"))
                    .put("quantity", newValue);
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.get_cart_item_wrt_cart_item_id));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.updateProductDetailsWRTCartItemId(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "updateProductDetailsWRTCartItemId: " + jsonObject);
                    Log.e(TAG, "getProductDetailsWRTCartItemId: code : " + SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE));
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        myViewHolder.recyclerCartActivityBinding.txtQty.setNumber(jsonObject.getString("quantity"));

                        if (newValue > oldValue) {
                            binding.txtCartTotalAmt.setText("$"+Utils.convertToUSDFormat(
                                    (Double.valueOf(binding.txtCartTotalAmt.getText().toString().replace("$",""))
                                            +jsonObject.getJSONObject("product").getDouble("price"))+""));

                            myViewHolder.recyclerCartActivityBinding.txtItemCost.setText("$"+Utils.convertToUSDFormat(
                                    (Double.valueOf(myViewHolder.recyclerCartActivityBinding.txtItemCost.getText().toString().replace("$",""))
                                            +jsonObject.getJSONObject("product").getDouble("price"))+""));
                        }else{
                            binding.txtCartTotalAmt.setText("$"+Utils.convertToUSDFormat(
                                    (Double.valueOf(binding.txtCartTotalAmt.getText().toString().replace("$",""))
                                            -jsonObject.getJSONObject("product").getDouble("price"))+""));

                            myViewHolder.recyclerCartActivityBinding.txtItemCost.setText("$"+Utils.convertToUSDFormat(
                                    (Double.valueOf(myViewHolder.recyclerCartActivityBinding.txtItemCost.getText().toString().replace("$",""))
                                            - jsonObject.getJSONObject("product").getDouble("price"))+""));
                        }
                    } else {
                        myViewHolder.recyclerCartActivityBinding.txtQty.setNumber(oldValue + "");
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                myViewHolder.recyclerCartActivityBinding.txtQty.setNumber(oldValue + "");
                e.printStackTrace();
            }
        });
    }

    private void deleteCart(JSONObject jsonObject) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.DELETE));  //delete method
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.delete_cart_item) + jsonObject.getString("cart_id") + "/?product_id=" + jsonObject.getString("product_id")+"&sequence_id="+jsonObject.getString("sequence_id"));
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.delete_cart_item));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.deleteProduct(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "deleteCart: " + responseObj);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        subtotal = 0;
                        Utils.getToast(context, getResources().getString(R.string.item_deleted));
                        int count = Integer.valueOf(SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT)) - 1;
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, String.valueOf(count));
                        showCartList();  // refresh cart
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    // get cart
    private void getCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*if (page == 1) {
            dialogCart = ProgressDialog.show(context, "Please wait", "Loading...");
        }*/
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getCart(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.e(TAG, "getCart: " + jsonObject);
                        JSONArray resultArray = jsonObject.getJSONArray("results");
                        if (resultArray.length() == 0) {
                            Utils.getToast(context, getResources().getString(R.string.no_items_in_cart));
                        } else {
                            JSONObject cartObj = resultArray.getJSONObject(0);
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, cartObj.getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CATEGORY_ID_WRT_CART, "restaurant");
                            showCartList();
                        }
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        JSONObject jsonObject = new JSONObject(response);
                        Utils.getToast(context, jsonObject.getString("msg"));  // internal server error
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                Utils.getToast(context, getResources().getString(R.string.no_response));
                e.printStackTrace();
            }
        });
    }

}
