package com.example.restaurantapp.activities.auth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.Preferences;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.activities.dashboard.DashBoardActivity;
import com.example.restaurantapp.databinding.ActivityMobileVerificationBinding;

import org.json.JSONObject;

public class MobileVerificationActivity extends AppCompatActivity {

    private ActivityMobileVerificationBinding binding;
    private AuthViewModel viewModel;
    private Context context;
    private String CountryID;
    public static final String TAG = MobileVerificationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_mobile_verification);
        viewModel = ViewModelProviders.of(this).get(AuthViewModel.class);
        context = MobileVerificationActivity.this;
        init();
    }

    private void init() {

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        CountryID= manager.getSimCountryIso().toUpperCase();
        Log.e(TAG, "init: "+CountryID );
        CountryID = (CountryID.equalsIgnoreCase("IN"))? "+91": "+1";
        Log.e(TAG, "init: "+CountryID );
        binding.edtCode.setText(CountryID);
        binding.btnSendOtp.setOnClickListener(v -> {
            if (!binding.edtMNo.getText().toString().equalsIgnoreCase("")) {
                if (VU.isConnectingToInternet(context)) {
                    getOTP();
                }
            }else Utils.getToast(context,getResources().getString(R.string.mb_validate));
        });

        binding.btnCancel.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });
    }

    protected void getOTP() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_otp);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("phone_number", binding.edtCode.getText().toString()+binding.edtMNo.getText().toString());
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getOTP(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getOTP: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        viewModel.dialogEnterOTP(context);
                        Utils.getToast(context, getResources().getString(R.string.otp_send_successfully));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    protected void checkOTP(String otp) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.check_otp);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("phone_number", binding.edtCode.getText().toString()+binding.edtMNo.getText().toString());
            stringMap.put("verification_code", otp);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.checkOTP(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "checkOTP: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.AUTHORIZATION_TOKEN, jsonObject.getString("token"));
                        UserSession.setIsuserLogin(context);
                        UserSession.setuserId(context, jsonObject.getString("id"));
                        Preferences.setUserProfile("2");
                        Preferences.setUserId(jsonObject.getString("id"));
                        Preferences.setToken(jsonObject.getString("token"));
                        Intent intent = new Intent(context, DashBoardActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        Utils.fadeAnimation(context);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
