package com.example.restaurantapp.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.Preferences;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.swa.RetrofitClient;
import com.example.restaurantapp.models.RetrarentRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUsActivity extends AppCompatActivity {


    private Context context;
    private Dialog dialog;
    public static final String TAG = AboutUsActivity.class.getSimpleName();
    TextView txt_about;
    ImageView img_about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        context = AboutUsActivity.this;

        //  WebView myWebView = binding.webviewAboutUs;
       // dialog = ProgressDialog.show(context, "Please wait", "Loading...");
//        splashTime();
//        WebSettings webSettings = myWebView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
//        myWebView.setWebViewClient(new WebViewClient());
//        myWebView.loadUrl(getResources().getString(R.string.about_us));
        txt_about = findViewById(R.id.txt_about);
        img_about= findViewById(R.id.img_about);
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("About Us");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        fetchdata();
    }
    void fetchdata() {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        //   API.postJson(new FooRequest("kit", "kat"));
        Call<RetrarentRequest> call = RetrofitClient.getInstance().getapi()
                .fetchdata("Token " + Preferences.getToken());
        call.enqueue(new Callback<RetrarentRequest>() {
            @Override
            public void onResponse(Call<RetrarentRequest> call, Response<RetrarentRequest> response) {



                if (response.code() == 200) {
                    RetrarentRequest p = response.body();
                    //  ArrayList<RetrarentResponse> s = new ArrayList<RetrarentResponse>(Arrays.asList(response.body().getResults()));
                    txt_about.setText(p.getDesc());
                    if  (p.getRestaurantUrl() != null) {
                        Utils.displayImageOriginalString(context, img_about, p.getRestaurantUrl());
                    }
                    dialog.dismiss();

                } else if (response.code() == 401){
                    dialog.dismiss();
                    Toast.makeText(AboutUsActivity.this, getResources().getString(R.string.credential_wrong), Toast.LENGTH_LONG).show();
                }

                else if (response.code() == 403){
                    dialog.dismiss();
                    Toast.makeText(AboutUsActivity.this, getResources().getString(R.string.email_not_verified), Toast.LENGTH_LONG).show();

                }

                else{
                    dialog.dismiss();
                    Toast.makeText(AboutUsActivity.this, getResources().getString(R.string.no_response), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<RetrarentRequest> call, Throwable t) {
                dialog.dismiss();
            }
        });


    }


    public void splashTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1300);
    }

}
