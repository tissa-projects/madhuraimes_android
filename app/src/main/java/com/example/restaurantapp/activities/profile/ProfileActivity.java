package com.example.restaurantapp.activities.profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.activities.password.ChangePasswordActivity;
import com.example.restaurantapp.databinding.ActivityProfileBinding;
import com.example.restaurantapp.interfaces.clickinterfaces.SetOnClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity implements SetOnClickListener.SetPasswordChange, SetOnClickListener , View.OnClickListener{

    private ActivityProfileBinding profileBinding;
    private ProfileViewModel profileViewModel;
    private static final int PICKER_REQUEST_CODE = 30;
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 12;
    private Context context;
    private ArrayList<String> countryCodeArray = null;
    private static final String TAG = ProfileActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        profileBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        context = ProfileActivity.this;
        init();
    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Profile");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        profileBinding.setChangePassword(this::onPasswordChangeClick);
        profileBinding.setSaveProfile(this::onCLick);

        if (VU.isConnectingToInternet(context)) {
            getProfile();
        }

        countryCodeArray = new ArrayList<>();
        countryCodeArray.add("+1");
        countryCodeArray.add("+91");

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
       String CountryID= manager.getSimCountryIso().toUpperCase();
        if (CountryID.equalsIgnoreCase("IN")){
            CountryID = "+91";
        }else {CountryID = "+1";}

        profileBinding.edtCode.setText(CountryID);
    }

    //countryCode dialog
    private void setCountryCode() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Salutation");
        // add a list
        builder.setItems(Utils.GetStringArray(countryCodeArray), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                profileBinding.edtCode.setText(countryCodeArray.get(position));
                dialog.dismiss();
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override// password change activity
    public void onPasswordChangeClick() {
        startActivity(new Intent(context, ChangePasswordActivity.class));
    }

    @Override  //save profile
    public void onCLick() {
        if (VU.isConnectingToInternet(context)) {
            if (profileViewModel.validate(context, profileBinding)) {
                updateUser();
            }
        }
    }

    private void getProfile() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_customer_api) + UserSession.getUserDetails(context).get("user_id");

        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_customer_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        profileViewModel.getProfile(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObject = new JSONObject(response);
                    Log.e(TAG, "getProfile: " + responseObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE)== 200) {
                        setProfileData(responseObject);
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void updateCustomer() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_customer_api)+UserSession.getUserDetails(context).get("user_id")+"/";

        try {
            JSONObject req = new JSONObject();
          //  req.put("id", UserSession.getUserDetails(context).get("user_id"));
            req.put("salutation", profileBinding.edtSalutation.getText().toString());
            req.put("first_name", profileBinding.fName.getText().toString());
            req.put("last_name", profileBinding.lName.getText().toString());
            req.put("username",profileBinding.username.getText().toString());
            req.put("email", profileBinding.email.getText().toString());
            req.put("last_access", Utils.CurrentTimeStamp());
            req.put("phone_number", profileBinding.edtCode.getText().toString()+profileBinding.edtMNo.getText().toString());
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.update_customer_api));
            helper.setUrlParameter(req.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        profileViewModel.updateProfile(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObject = new JSONObject(response);
                    Log.e(TAG, "updateProfile: " + responseObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        Utils.getToast(context,"Profile updated successfully");
                       /* JSONObject dataObj = responseObject.getJSONObject("data");
                        profileBinding.fName.setText(dataObj.getString("first_name"));
                        profileBinding.lName.setText(dataObj.getString("last_name"));
                        profileBinding.email.setText(dataObj.getString("email"));*/

                    } else {
                        CustomDialogs.dialogShowMsg(context, responseObject.getJSONObject("data").getString("msg"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void updateUser() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_user_api)+UserSession.getUserDetails(context).get("user_id")+"/";

        try {
            JSONObject req = new JSONObject();
          //  req.put("id", UserSession.getUserDetails(context).get("user_id"));
            req.put("salutation", profileBinding.edtSalutation.getText().toString());
            req.put("first_name", profileBinding.fName.getText().toString());
            req.put("last_name", profileBinding.lName.getText().toString());
            req.put("username",profileBinding.username.getText().toString());
            req.put("email", profileBinding.email.getText().toString());
            req.put("last_access", Utils.CurrentTimeStamp());
            req.put("phone_number", profileBinding.edtCode.getText().toString()+profileBinding.edtMNo.getText().toString());
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.update_user_api));
            helper.setUrlParameter(req.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        profileViewModel.updateProfile(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObject = new JSONObject(response);
                    Log.e(TAG, "updateProfile: " + responseObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        updateCustomer();
                       /* JSONObject dataObj = responseObject.getJSONObject("data");
                        profileBinding.fName.setText(dataObj.getString("first_name"));
                        profileBinding.lName.setText(dataObj.getString("last_name"));
                        profileBinding.email.setText(dataObj.getString("email"));*/

                    } else {
                        CustomDialogs.dialogShowMsg(context, responseObject.getJSONObject("data").getString("msg"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setProfileData(JSONObject jsonObject) {
        try {
            JSONArray resultArray = jsonObject.getJSONArray("results");
            Log.e(TAG, "getCustomer: " + resultArray.toString());
            JSONObject resultObj = resultArray.getJSONObject(0);
            JSONObject customerObj = resultObj.getJSONObject("customer");
            String strMNo = null;
            if (resultObj.getString("phone_number").contains("+1")){
                strMNo = resultObj.getString("phone_number").replace("+1","");
            }else {strMNo = resultObj.getString("phone_number").replace("+91","");}
            profileBinding.edtMNo.setText(strMNo);
            profileBinding.username.setText(customerObj.getString("username"));
            profileBinding.fName.setText(customerObj.getString("first_name"));
            profileBinding.lName.setText(customerObj.getString("last_name"));
            profileBinding.email.setText(customerObj.getString("email"));
            profileBinding.edtSalutation.setText(resultObj.getString("salutation"));
            UserSession.createUserLoginSession(context,customerObj.getString("first_name"),customerObj.getString("last_name"),
                    resultObj.getString("salutation"),strMNo,customerObj.getString("email"),
                    customerObj.getString("username"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edt_code:
                setCountryCode();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.fadeAnimation(context);
    }
}
