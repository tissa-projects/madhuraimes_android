package com.example.restaurantapp.activities.myorderdetails;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.databinding.ActivityMyOrderDetailsBinding;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyOrderDetailsActivity extends AppCompatActivity {

    private ActivityMyOrderDetailsBinding myOrderDetailsBinding;
    private MyOrderDetailsAdapter adapter;
    private MyOrderDetailsViewModel myOrderDetailsViewModel;
    private Context context;
    private String strOrderId, strOrderDate;
    private static final String TAG = MyOrderDetailsActivity.class.getSimpleName();
    private Dialog dialog, dialogAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myOrderDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_order_details);
        myOrderDetailsViewModel = ViewModelProviders.of(this).get(MyOrderDetailsViewModel.class);
        context = MyOrderDetailsActivity.this;
        getIntentData();
        init();
        if (VU.isConnectingToInternet(context)) {
            getOrderDetails(strOrderId);
            getShippingAddress();
        }

    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                String data = bundle.getString("jsonData");
                JSONObject jsonData = new JSONObject(data);
                Log.e(TAG, "getIntentData: "+jsonData );
                JSONObject bundleData = jsonData.getJSONObject("order");
                JSONObject shippingObj = jsonData.getJSONObject("shippingmethod");
                JSONObject restaurantObj = jsonData.getJSONObject("restaurant");
             //   JSONObject storeObj = jsonData.getJSONObject("store");
                Log.e(TAG, "getIntentData: jsonObject: " + jsonData);
                //    strPaymentId = bundleData.getString("payment_id");
                //  strPaymentType = bundleData.getString("payment_method");
                strOrderId = bundleData.getString("order_id");
                // strOrderTotal= bundleData.getJSONObject("order").getString("total");
                strOrderDate = Utils.ConvertIntoDateFormat("yyyy-MM-dd", "dd MMMM yyyy", bundleData.getString("created_at"));
                String currencyType = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE);
                myOrderDetailsBinding.txtOrderDate.setText(strOrderDate);
                myOrderDetailsBinding.txtSubTotal.setText(currencyType + bundleData.getString("subtotal"));
                myOrderDetailsBinding.txtOrderNo.setText(bundleData.getString("order_id"));
                myOrderDetailsBinding.txtTip.setText(currencyType + bundleData.getString("tip"));
                myOrderDetailsBinding.txtTax.setText(currencyType + bundleData.getString("tax"));
                myOrderDetailsBinding.txtDiscount.setText(currencyType + bundleData.getString("discount"));
                myOrderDetailsBinding.txtOrderTotal.setText(currencyType + bundleData.getString("total"));
                myOrderDetailsBinding.txtPaymtMethod.setText(jsonData.getString("payment_method"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Order Details");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        adapter = new MyOrderDetailsAdapter(context);
        myOrderDetailsBinding.setMyOrderDetailsAdapter(adapter);

    }

    private void getShippingAddress() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_billing_address_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_billing_address_api));
            helper.setOrderId(strOrderId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogAddress = ProgressDialog.show(context, "Please wait", "Loading...");
        myOrderDetailsViewModel.getBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogAddress.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getShippingAddress: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray resultArray = jsonObject.getJSONArray("results");
//                        getJSONObject("data").
                        if (resultArray.length() > 0) {
                            JSONObject resultObj = resultArray.getJSONObject(0);
                            myOrderDetailsBinding.txtShippingAddress.setText(resultObj.getString("name") + "\n"
                                    +resultObj.getString("address") + " "+resultObj.getString("house_number")+"\n" +
                                    resultObj.getString("city") + "\n" +
                                    resultObj.getString("state") + "\n" +
                                    resultObj.getString("country") + "\n" +
                                    resultObj.getString("zip") + "\n");
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getOrderDetails(String strOrderId) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_order_details_api) + strOrderId;
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_order_details_api));
            helper.setOrderId(strOrderId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        myOrderDetailsViewModel.getMyOrderDetails(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getStoreList: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        if (jsonArray.length() > 0) {
                            adapter.setData(jsonArray);
                        } else {
                            Utils.getToast(context, getResources().getString(R.string.no_product_available_for_order));
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

}
