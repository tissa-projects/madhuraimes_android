package com.example.restaurantapp.activities.auth;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.Utilities.Validations;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.databinding.ActivityLoginBinding;
import com.example.restaurantapp.databinding.ActivitySignUpBinding;


public class AuthViewModel extends AndroidViewModel {

    public static final String TAG = AuthViewModel.class.getSimpleName();
    private AuthRepository authRepository;

    public AuthViewModel(@NonNull Application application) {
        super(application);
        authRepository = AuthRepository.getInstance(application);
    }

    public LiveData<String> login(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.getLoginData(restAPIClientHelper);

    }

    public LiveData<String> getOTP(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.getOTP(restAPIClientHelper);

    }

    public LiveData<String> checkOTP(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.checkOTP(restAPIClientHelper);

    }

    public MutableLiveData<String> logout(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.logout(restAPIClientHelper);
    }

    public LiveData<String> forgotPasword(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.forgotPassword(restAPIClientHelper);
    }

    public MutableLiveData<String> getMenuList(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.getMenuList(restAPIClientHelper);
    }

    public MutableLiveData<String> getSpecialMenu(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.getSpecialMenu(restAPIClientHelper);
    }
    public MutableLiveData<String> getSearchData(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.getSpecialMenu(restAPIClientHelper);
    }

    public LiveData<String> getCustomer(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.getCustomer(restAPIClientHelper);
    }

    public LiveData<String> getCartCount(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.getCartCount(restAPIClientHelper);
    }

    public boolean validateLogin(Context context, ActivityLoginBinding loginBinding) {
        if (loginBinding.edtEmail.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.enter_email_validate));
            return false;
        } else if (loginBinding.edtPassword.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.enter_password_validate));
            return false;
        }
        return true;
    }


    // ===========================  Registration =================================================

    public MutableLiveData<String> setRegistrationData(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.setRegistrationData(restAPIClientHelper);
    }

    public LiveData<String> createCustomer(RestAPIClientHelper restAPIClientHelper) {
        return authRepository.createCustomer(restAPIClientHelper);
    }

    public boolean validateRegistartionn(Context context, ActivitySignUpBinding registrationBinding) {
        if (registrationBinding.edtSalutation.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.salutation_validate));
            return false;
        } else if (registrationBinding.edtUserName.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.username_validate));
            return false;
        } else if (registrationBinding.edtFName.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.fname_validate));
            return false;
        } else if (registrationBinding.edtLName.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.lname_validate));
            return false;
        } else if (registrationBinding.edtEmail.getText().toString().equalsIgnoreCase("") || Validations.isEnailValid(registrationBinding.edtEmail.getText().toString())) {
            Utils.getToast(context, context.getResources().getString(R.string.enter_email_validate));
            return false;
        } else if (registrationBinding.edtMNo.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.mb_validate));
            return false;
        } else if (registrationBinding.edtPassword.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.password_validate));
            return false;
        } else if (registrationBinding.edtPassword.getText().toString().length() <= 8) {
            Utils.getToast(context, context.getResources().getString(R.string.password_validate_character));
            return false;
        } else if (registrationBinding.edtCPassword.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.confrm_password_validate));
            return false;
        } else if (!registrationBinding.edtCPassword.getText().toString().equalsIgnoreCase(registrationBinding.edtPassword.getText().toString())) {
            Utils.getToast(context, context.getResources().getString(R.string.password_not_match_validate));
            return false;
        }
        return true;
    }

    //============================================dialogs ===========================
    public void dialogForgotPasswordUser(String type, Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_password_reset);

        EditText edtEmail = dialog.findViewById(R.id.txt_msg);

        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        if (type.equalsIgnoreCase("password"))
            edtEmail.setHint("Enter your username");
        else
            edtEmail.setHint("Enter your email");
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener((view) -> {
            if (VU.isConnectingToInternet(context)) {
                if (!edtEmail.getText().toString().equalsIgnoreCase("")) {
                    if (type.equalsIgnoreCase("password"))
                        ((LoginActivity) context).ForgotPassowrd(edtEmail.getText().toString());
                    else
                        ((LoginActivity) context).ForgotUser(edtEmail.getText().toString());
                    dialog.dismiss();
                } else if (type.equalsIgnoreCase("password"))
                    Utils.getToast(context, context.getResources().getString(R.string.pass_username));
                else
                    Utils.getToast(context, context.getResources().getString(R.string.pass_email));
            }
        });

        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener((view) -> {
            dialog.dismiss();
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }




    protected Dialog dialogEnterOTP(Context context) {
        Dialog dialog = new Dialog(context);
        try {
            dialog.setContentView(R.layout.dialog_with_editbox);
            final EditText edtOTP = (EditText) dialog.findViewById(R.id.edt_text);
            final Button btnSubmit = ((Button) dialog.findViewById(R.id.bt_submit));
            btnSubmit.setText("Submit");
            edtOTP.setHint("Enter the OTP");
            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            btnSubmit.setOnClickListener((view) -> {
                if (VU.isConnectingToInternet(context)) {
                    Log.e(TAG, "dialogEnterOTP: " + edtOTP.getText().toString());
                    if (edtOTP.getText().toString().equalsIgnoreCase("")) {
                        Utils.getToast(context, context.getResources().getString(R.string.enter_otp_validate));
                    } else {
                        ((MobileVerificationActivity) context).checkOTP(edtOTP.getText().toString());
                        dialog.dismiss();
                    }
                }
            });

            ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(v -> dialog.dismiss());
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            dialog.getWindow().setAttributes(lp);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dialog;
    }


}
