package com.example.restaurantapp.activities.payment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.databinding.ActivityPaymentBinding;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityPaymentBinding paymentBinding;
    private PaymentViewModel paymentViewModel;
    private Context context;
    private String currencyType = "$";
    private JSONArray cartDataArray = null;
    private JSONObject addressObj = null;
    private String tip = "0", customTip = "0", shippingId = "0", shippingMethdName, strCounntryShortCode, subTotal = "0"/*, noTaxTotal = "0"*/;
    public static final String TAG = PaymentActivity.class.getSimpleName();

    String orderid;
    JSONObject orderdetail;
    int count=0;
    String firstnamelast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paymentBinding = DataBindingUtil.setContentView(this, R.layout.activity_payment);
        paymentViewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
        context = PaymentActivity.this;
        getIntentData();
        if (VU.isConnectingToInternet(context)) {
            addFees();
            getShippingMethods();
        }
        init();
    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Payment");

        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());

        paymentBinding.serviceFeeNote.setOnClickListener(v -> {
            new SimpleTooltip.Builder(this)
                    .anchorView(paymentBinding.serviceFeeNote)
                    .text(getResources().getString(R.string.service_fee_note))
                    .gravity(Gravity.TOP)
                    .animated(true)
                    .transparentOverlay(false).textColor(Color.WHITE)
                    .build()
                    .show();
            //  CustomDialogs.dialogShowMsg(context, getResources().getString(R.string.service_fee_note));
        });

        paymentBinding.edtMonth.setOnClickListener(this::onClick);
        paymentBinding.edtYear.setOnClickListener(this::onClick);
        paymentBinding.btnNext.setOnClickListener(v -> {
            if (VU.isConnectingToInternet(context)) {
                if (paymentViewModel.validate(context, paymentBinding)) {
                    if (count==0){
                        setOrderDetails();
                        count++;
                    }else {
                        payment();
                    }



                }
            }
        });

        paymentBinding.txtChangeTip.setOnClickListener(v -> {
            dialogCustomTip();
        });
        getProfile();

    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                Log.e(TAG, "getIntentData: bundle: " + bundle.toString());
                String data = bundle.getString("bundle");
                String data1 = bundle.getString("cartDataArray");
                strCounntryShortCode = bundle.getString("counntryShortCode");
                JSONObject bundleData = new JSONObject(data);
                Log.e(TAG, "getIntentData: data1: " + data1);
                cartDataArray = new JSONArray(data1);
                Log.e(TAG, "getIntentData: " + cartDataArray.toString());

                String strAddress = bundleData.getString("addressObj");
                subTotal = bundle.getString("subTotal");
                addressObj = new JSONObject(strAddress);
                Log.e(TAG, "onCreate: addressObj: " + addressObj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edt_month:
                datePicker("Month");
                break;
            case R.id.edt_year:
                datePicker("Year");
                break;
        }
    }


    private void datePicker(String type) {
        final Calendar myCalendar = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(context, new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                Log.d(TAG, "selectedMonth : " + selectedMonth + " selectedYear : " + selectedYear);
                String strSelectedMonth = String.valueOf(selectedMonth).length() > 1 ? (selectedMonth + 1) + "" : "0" + (selectedMonth + 1);
                Log.e(TAG, "onDateSet: +strSelectedMonth: " + strSelectedMonth);
                paymentBinding.edtYear.setText(selectedYear + "");
                paymentBinding.edtMonth.setText(strSelectedMonth);
            }
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH));

        builder.setActivatedMonth(Calendar.MONTH)
                .setMinYear(1990)
                .setActivatedYear(myCalendar.get(Calendar.YEAR))
                .setMaxYear(2030)
                .setMinMonth(Calendar.JANUARY)
                .setTitle("Select " + type)
                .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)

                // .setMaxMonth(Calendar.OCTOBER)
                // .setYearRange(1890, 1890)
                // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
                //.showMonthOnly()
                // .showYearOnly()
                .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                    @Override
                    public void onMonthChanged(int selectedMonth) {
                        Log.d(TAG, "Selected month : " + selectedMonth);
                        String strSelectedMonth = selectedMonth + "".length() > 1 ? (selectedMonth + 1) + "" : "0" + (selectedMonth + 1);
                        paymentBinding.edtMonth.setText(strSelectedMonth);
                    }
                })
                .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                    @Override
                    public void onYearChanged(int selectedYear) {
                        Log.d(TAG, "Selected year : " + selectedYear);
                        paymentBinding.edtYear.setText(selectedYear + "");
                    }
                })
                .build()
                .show();
    }


    public void dialogCustomTip() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_delevery_tip);
        RadioGroup radioGroup = ((RadioGroup) dialog.findViewById(R.id.radio_grup));
        EditText edtTip = ((EditText) dialog.findViewById(R.id.edt_tip));
        ((RadioButton) dialog.findViewById(R.id.amt_1)).setText("15%");
        ((RadioButton) dialog.findViewById(R.id.amt_2)).setText("20%");
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) dialog.findViewById(checkedId); // find the radiobutton by returned id
                if (radioButton.getText().toString().equals("Other amount")) {
                    edtTip.setEnabled(true);
                } else {
                    edtTip.setEnabled(false);
                }
            }
        });

        ((Button) dialog.findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
                if (selectedId == -1) {
                    Utils.getToast(context, getResources().getString(R.string.select_tip_option));
                } else {
                    // find the radiobutton by returned id
                    RadioButton radioButton = (RadioButton) dialog.findViewById(selectedId);
                    if (radioButton.getText().equals("Other amount")) {
                        if (edtTip.getText().toString().equals("")) {
                            Utils.getToast(context, "Please enter Other amount");

                        } else {
                            paymentBinding.txtTip.setText(currencyType + edtTip.getText().toString());
                            customTip = edtTip.getText().toString();
                            tip = "0";
                            if (VU.isConnectingToInternet(context)) {
                                addFees();
                            }
                            dialog.dismiss();
                        }
                    } else {
                        if (radioButton.getText().toString().contains(currencyType)) {
                            paymentBinding.txtTip.setText(radioButton.getText().toString());
                            customTip = radioButton.getText().toString().substring(1);
                            tip = "0";
                        } else {
                            paymentBinding.txtTip.setText(currencyType + radioButton.getText().toString().split("%")[0]);
                            customTip = "0";
                            tip = radioButton.getText().toString().split("%")[0];
                        }
                        dialog.dismiss();
                        if (VU.isConnectingToInternet(context)) {
                            addFees();
                        }
                    }

                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void addFees() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        //   String tip = (cartBinding.txtTip.getText().toString().equalsIgnoreCase("")) ? "0" : cartBinding.txtTip.getText().toString();
        try {
            JSONObject reqObj = new JSONObject();
            reqObj.put("sub_total", subTotal);
            reqObj.put("no_tax_total", 0);
            reqObj.put("restaurant_id", "1");
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));


            if (customTip.equalsIgnoreCase("0")) {
                reqObj.put("tip", tip);  // %
            } else {
                reqObj.put("custom_tip", customTip); // normal value
            }
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.add_fees));
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.add_fees));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        paymentViewModel.addFee(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "addFees: " + responseObj);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        //  responseObj = responseObj.getJSONObject("data");
                        paymentBinding.txtSubTotal.setText(currencyType + responseObj.getString("sub_total"));
                        paymentBinding.txtVat.setText(currencyType + responseObj.getString("tax"));
                        paymentBinding.txtTip.setText(currencyType + responseObj.getString("tip"));
                     //   paymentBinding.txtServiceFee.setText(currencyType + responseObj.getString("service_fee"));
                        paymentBinding.txtDiscount.setText(currencyType + responseObj.getString("discount"));
                        paymentBinding.txtTotal.setText(currencyType + responseObj.getString("total"));
                        //  startActivity(new Intent(context, CheckOutActivity.class));
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        Utils.getToast(context, responseObj.getString("message"));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }


    private void getProfile() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_customer_api) + UserSession.getUserDetails(context).get("user_id");

        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_customer_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        paymentViewModel.getProfile(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObject = new JSONObject(response);
                    Log.e(TAG, "getProfile: " + responseObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE)== 200) {
                        setProfileData(responseObject);
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setProfileData(JSONObject jsonObject) {
        try {
            JSONArray resultArray = jsonObject.getJSONArray("results");
            Log.e(TAG, "getCustomer: " + resultArray.toString());
            JSONObject resultObj = resultArray.getJSONObject(0);
            JSONObject customerObj = resultObj.getJSONObject("customer");
            String strMNo = null;
            firstnamelast=customerObj.getString("first_name")+ " "+customerObj.getString("last_name");
            if (resultObj.getString("phone_number").contains("+1")){
                strMNo = resultObj.getString("phone_number").replace("+1","");
            }else {strMNo = resultObj.getString("phone_number").replace("+91","");}

            UserSession.createUserLoginSession(context,customerObj.getString("first_name"),customerObj.getString("last_name"),
                    resultObj.getString("salutation"),strMNo,customerObj.getString("email"),
                    customerObj.getString("username"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setOrderDetails() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_order_details_api);
        try {
            reqObj.put("status", "active");  //customer always be active
            reqObj.put("currency", "USD");
            reqObj.put("cart_id", SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID));
            reqObj.put("subtotal", paymentBinding.txtSubTotal.getText().toString().substring(1));
            reqObj.put("total", paymentBinding.txtTotal.getText().toString().substring(1));
            reqObj.put("extra", paymentBinding.edtExtraAnnotation.getText().toString());
            reqObj.put("customer", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("tip", paymentBinding.txtTip.getText().toString().substring(1));
       //   reqObj.put("service_fee", paymentBinding.txtServiceFee.getText().toString().substring(1));
          reqObj.put("service_fee", "0");
            reqObj.put("tax", paymentBinding.txtVat.getText().toString().substring(1));
            reqObj.put("discount", paymentBinding.txtDiscount.getText().toString().substring(1));
            reqObj.put("shipping_fee", 0);

            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.set_order_details_api));
        } catch (Exception e) {
            e.printStackTrace();

        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        paymentViewModel.setOrder(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setOrderDetails: " + jsonObject);
                    JSONObject dataObj = jsonObject;
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        //OrderDetails = dataObj;
                        Log.e(TAG, "setOrderDetails: " + dataObj.toString());
                        orderid=dataObj.getString("order_id");
                        orderdetail=dataObj;
                       payment();//order id
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        CustomDialogs.dialogShowMsg(context, dataObj.getString("msg"));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
            }
        });
    }

    private void payment() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        JSONObject reqObj = null;
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.payment_api);
        try {
            String currency = "USD";
            reqObj = new JSONObject();
            JSONObject cardObj = new JSONObject();
            JSONObject billingDetails = new JSONObject();
            JSONObject metadata = new JSONObject();
            JSONObject address = new JSONObject();
            Log.e(TAG, "payment: addressObj: " + addressObj.toString());

            metadata.put("shippingmethod_id",shippingId).put("order_id", orderid)
                    .put("phone", UserSession.getUserDetails(context).get(UserSession.KEY_M_NO))
                    .put("restaurant_id", getResources().getString(R.string.restaurant))
                    .put("name", firstnamelast)
                    .put("special_instruction", paymentBinding.edtExtraAnnotation.getText().toString())

                    .put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            address.put("city", addressObj.getString("city")).put("country", strCounntryShortCode)
                    .put("line1", addressObj.getString("address")).put("line2", "")
                    .put("postal_code", addressObj.getString("zip")).put("state", addressObj.getString("state"));
            billingDetails.put("address", address);
            cardObj.put("number", paymentBinding.edtCreditCardNo.getText().toString())
                    .put("exp_month", paymentBinding.edtMonth.getText().toString())
                    .put("exp_year", paymentBinding.edtYear.getText().toString())
                    .put("cvc", paymentBinding.edtSecurityCode.getText().toString());
            reqObj.put("amount", paymentBinding.txtTotal.getText().toString().substring(1))
                    .put("currency", currency).put("receipt_email", UserSession.getUserDetails(context).get(UserSession.KEY_EMAIL))
                    .put("type", "card").put("card", cardObj)
                    .put("billing_details", billingDetails).put("metadata", metadata);
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.payment_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        paymentViewModel.payment(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "payment: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONObject resultObj = jsonObject;
                        Log.e(TAG, "payment: " + resultObj.toString());
                        Utils.getToast(context, getResources().getString(R.string.order_placed));
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                        Intent intent = new Intent(context, BackToStoreActivity.class)
                                .putExtra("OrderDetails", orderdetail.toString())
                                .putExtra("addressDetails", addressObj.toString());
                        startActivity(intent);
                        //showCartList(resultObj);   //for adding cart items to order item after payment success
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 400) {
                        Utils.getToast(context, getResources().getString(R.string.incorrect_cart_details));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                e.printStackTrace();
            }
        });
    }

    private void getShippingMethods() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_shipping_methods) + "status=ACTIVE&restaurant_id="+getResources().getString(R.string.restaurant));
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_shipping_methods));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        paymentViewModel.getShippingFees(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getShippingMethods: " + responseObj);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray jsonArray = responseObj.getJSONArray("results");
                        Log.e(TAG, "getShippingMethods: "+jsonArray );
                        shippingId = jsonArray.getJSONObject(0).getString("id");
                        if (jsonArray.length() == 0) {
                            Utils.getToast(context, "No shipping method available");
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.fadeAnimation(context);
        finish();
    }
}
