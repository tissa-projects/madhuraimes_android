package com.example.restaurantapp.activities.dashboard;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.models.CategoryMenuModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class DashboardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // private static final String[]CAT_IMAGE_IDS = new String[]{
    //       "Soups","Tiffins","Appetizers","Indo-Chinese Menu","Drinks","Egg Appetizers","Thali","Kababs","South Indian Entree"};
    private Context context;
    private JSONArray jsonArray;
    private final int VIEW_ITEM = 1;
    private final int VIEW_SECTION = 0;
    private OnItemClickListener mOnItemClickListener;
    public static final String TAG = DashboardAdapter.class.getSimpleName();


    public interface OnItemClickListener {
        void onItemClick(JSONObject obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public DashboardAdapter(Context context) {
        this.context = context;
        jsonArray = new JSONArray();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      /*  return new MyViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.recycler_dashboard_item_list, parent, false));*/

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_menu_list, parent, false);
            vh = new OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section, parent, false);
            vh = new SectionViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            //People p = items.get(position);
            JSONObject jsonObject = jsonArray.getJSONObject(position);
            if (holder instanceof OriginalViewHolder) {
                OriginalViewHolder view = (OriginalViewHolder) holder;
                String imgUrl = "";
                view.name.setText(jsonObject.getString("productName"));
                imgUrl = jsonObject.getString("productUrl");
                if ((!imgUrl.equalsIgnoreCase("")) || (imgUrl != null)) {
                    Utils.displayImageOriginalString(context, view.image, imgUrl);
                }
                view.price.setText(SharePreferenceUtil.getSPstringValue(context,
                        SharePreferenceUtil.CURRENCY_TYPE) + jsonObject.getString("price"));
                view.menuDesc.setText(jsonObject.getString("extra"));

                if (((DashBoardActivity)context).getRestaurentStatus().equalsIgnoreCase("closed")){
                 //   view.lyt_parent.setBackgroundColor(context.getResources().getColor(R.color.grey_20));
                    view.lyt_parent.setAlpha(Float.valueOf((float) 0.3));
                }else {
                    view.ml_parent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mOnItemClickListener != null) {
                                mOnItemClickListener.onItemClick(jsonObject, position);
                            }
                        }
                    });
                }
            } else {

                SectionViewHolder view = (SectionViewHolder) holder;
                view.title_section.setText(jsonObject.getString("categoryName"));
                if (((DashBoardActivity)context).getRestaurentStatus().equalsIgnoreCase("closed")){
                    //   view.lyt_parent.setBackgroundColor(context.getResources().getColor(R.color.grey_20));
                    view.title_section.setAlpha(Float.valueOf((float) 0.3));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearData() {
        jsonArray = null;
        jsonArray = new JSONArray();
    }

    public void setSpecialMenu(JSONArray jsonArray) {

        try {
            if (jsonArray.length() > 0) {
                this.jsonArray.put(new JSONObject()
                        .put("categoryName", "Special Menu")  //category name
                        .put("section", true));
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject menuObj = jsonArray.getJSONObject(i);
                    jsonObject.put("price", menuObj.getString("price"));
                    jsonObject.put("section", false);
                    jsonObject.put("productName", menuObj.getString("product_name"));
                    jsonObject.put("productId", menuObj.getString("product_id"));
                    jsonObject.put("productUrl", menuObj.getString("product_url"));
                    jsonObject.put("extra", menuObj.getString("extra"));
                    this.jsonArray.put(jsonObject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    public void setData(JSONArray jsonArray, Map<Integer, CategoryMenuModel> categoryMenuModelMap, String type) {
        Log.e(TAG, "setData: " + jsonArray.toString());
        try {
            if (jsonArray.length() > 0) {
                if (type.equalsIgnoreCase("menu_list")) {
                    this.jsonArray.put(new JSONObject()
                            .put("categoryName", categoryMenuModelMap.get(jsonArray.getJSONObject(0).getInt("category"))
                                    .getJsonObject().getString("category"))  //category name
                            .put("section", true));
                } else if (type.equalsIgnoreCase("search")) {
                    this.jsonArray.put(new JSONObject()
                            .put("categoryName", categoryMenuModelMap.get(jsonArray.getJSONObject(0).getJSONObject("category").getInt("categoryId"))
                                    .getJsonObject().getString("category"))  //category name
                            .put("section", true));
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject menuObj = jsonArray.getJSONObject(i);
                    jsonObject.put("price", menuObj.getString("price"));
                    jsonObject.put("extra", menuObj.getString("extra"));
                    jsonObject.put("section", false);
                    if (menuObj.has("product_name")) {
                        jsonObject.put("productName", menuObj.getString("product_name"));
                    } else {
                        jsonObject.put("productName", menuObj.getString("productName"));
                    }
                    if (menuObj.has("product_id")) {
                        jsonObject.put("productId", menuObj.getString("product_id"));
                    } else {
                        jsonObject.put("productId", menuObj.getString("productId"));
                    }

                    if (menuObj.has("product_url")) {
                        jsonObject.put("productUrl", menuObj.getString("product_url"));
                    } else {
                        jsonObject.put("productUrl", menuObj.getString("productUrl"));
                    }

                    if (i < jsonArray.length() - 2) {
                        if (type.equalsIgnoreCase("menu_list")) {
                            if (menuObj.getInt("category") == jsonArray.getJSONObject(i + 1).getInt("category")) {
                                this.jsonArray.put(jsonObject);
                            } else {
                                this.jsonArray.put(jsonObject);
                                CategoryMenuModel categoryMenuModel = categoryMenuModelMap.get(jsonArray.getJSONObject(i + 1).getInt("category"));
                                this.jsonArray.put(new JSONObject()
                                        .put("categoryName", categoryMenuModel.getJsonObject().getString("category"))  //category name
                                        .put("section", true));
                            }
                        } else if (type.equalsIgnoreCase("search")) {
                            if (menuObj.getJSONObject("category").getInt("categoryId") == jsonArray.getJSONObject(i + 1).getJSONObject("category").getInt("categoryId")) {
                                this.jsonArray.put(jsonObject);
                            } else {
                                this.jsonArray.put(jsonObject);
                                CategoryMenuModel categoryMenuModel = categoryMenuModelMap.get(jsonArray.getJSONObject(i + 1).getJSONObject("category").getInt("categoryId"));
                                this.jsonArray.put(new JSONObject()
                                        .put("categoryName", categoryMenuModel.getJsonObject().getString("category"))  //category name
                                        .put("section", true));
                            }
                        }
                    } else {
                        this.jsonArray.put(jsonObject);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        int section = 0;
        try {
            section = this.jsonArray.getJSONObject(position).getBoolean("section") ? VIEW_SECTION : VIEW_ITEM;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return section;
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView name, price,menuDesc;
        public View lyt_parent;
        public View ml_parent;

        public OriginalViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
            name = (TextView) v.findViewById(R.id.menu_name);
            price = (TextView) v.findViewById(R.id.price);
            menuDesc = (TextView) v.findViewById(R.id.menu_desc);
            lyt_parent = (View) v.findViewById(R.id.lyt_parent);
            ml_parent = (View) v.findViewById(R.id.ml_parent);
        }
    }

    public static class SectionViewHolder extends RecyclerView.ViewHolder {
        public TextView title_section;

        public SectionViewHolder(View v) {
            super(v);
            title_section = (TextView) v.findViewById(R.id.title_section);
        }
    }

   /* class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView mImageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.text);

        }
    }*/
}
