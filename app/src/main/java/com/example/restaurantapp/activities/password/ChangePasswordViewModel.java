package com.example.restaurantapp.activities.password;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.databinding.ActivityChangePasswordBinding;

public class ChangePasswordViewModel extends AndroidViewModel {
    private ChangePasswordRepository repository;

    public ChangePasswordViewModel(@NonNull Application application) {
        super(application);
        repository = ChangePasswordRepository.getInstance(application);
    }

    public MutableLiveData<String> changePassoword(RestAPIClientHelper restAPIClientHelper) {
        return repository.changePassoword(restAPIClientHelper);
    }


    public boolean validate(Context context, ActivityChangePasswordBinding changePasswordBinding) {
        if (changePasswordBinding.currentPassword.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.current_password_validate));
            return false;
        } else if (changePasswordBinding.newPassword.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.new_password_validate));
            return false;
        }else if (changePasswordBinding.newPassword.getText().toString().length()<=8){
            Utils.getToast(context,context.getResources().getString(R.string.password_validate_character));
            return  false;
        } else if (changePasswordBinding.confirmPassword.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.confrm_password_validate));
            return false;
        }else if (!changePasswordBinding.newPassword.getText().toString().equalsIgnoreCase(changePasswordBinding.confirmPassword.getText().toString())){
            Utils.getToast(context,context.getResources().getString(R.string.password_change_validate));
            return  false;
        }
        return true;
    }

    }
