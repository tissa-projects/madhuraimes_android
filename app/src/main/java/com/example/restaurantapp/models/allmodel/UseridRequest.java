package com.example.restaurantapp.models.allmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UseridRequest {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("next")
    @Expose
    private Object next;
    @SerializedName("previous")
    @Expose
    private Object previous;
    @SerializedName("results")
    @Expose
    private UseridResponse[] results = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Object getNext() {
        return next;
    }

    public void setNext(Object next) {
        this.next = next;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public UseridResponse[] getResults() {
        return results;
    }

    public void setResults(UseridResponse[] results) {
        this.results = results;
    }
}
