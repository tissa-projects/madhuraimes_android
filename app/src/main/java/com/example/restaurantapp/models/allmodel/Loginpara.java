package com.example.restaurantapp.models.allmodel;

public class Loginpara {
    final String username;
    final String password;
    final String restaurant_id;


    public Loginpara(String username, String password, String restaurant_id) {
        this.username = username;
        this.password = password;
        this.restaurant_id = restaurant_id;
    }
}
